TZ-7 Project
=======


EVE Online API and 3rd Party access manager tool.

**Under development**


### For main modules read: ###

* [Tz7\EveApiBundle](https://bitbucket.org/adamus/tz7eveapibundle)
* [Tz7\EveCriterionBundle](https://bitbucket.org/adamus/tz7evecriterionbundle)
* [Tz7\EveDiscordBundle](https://bitbucket.org/adamus/tz7evediscordbundle)


### TODOs: ###

* Separate TeamSpeak3 Bundle
* Generated menus
* Language switcher
* More API functions


### History: ###

TZ-7 stands for "TorK-Zone 7", as the 7th EVE Online tool by Adamus TorK

The whole series of project was started with fuel calculations for mining towers, 
then TZ-6, alias RAXAPI became also a security tool checking member access for TeamSpeak3 
and logging member movements and activity.

With the close of HUN Reloaded, the TZ-7 is planned to be an open source, open service project 
and public data access for [https://eve-online.hu](https://eve-online.hu)

TZ-7 should be able to provide services for multiple entities (Corporation, Alliances) 
in the near future.


If you want to support me, send ISK to Kathryn Darly, so I can develop while she blowing up ships :trollface:
