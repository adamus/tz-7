<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20151229173332 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE ts_sever (id INT AUTO_INCREMENT NOT NULL, host VARCHAR(64) NOT NULL, username VARCHAR(255) NOT NULL, password VARCHAR(255) NOT NULL, query_port SMALLINT NOT NULL, client_port SMALLINT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ts_criterion (id INT AUTO_INCREMENT NOT NULL, server_id INT DEFAULT NULL, group_id INT DEFAULT NULL, comparison_type SMALLINT NOT NULL, condition_value VARCHAR(255) NOT NULL, match_type SMALLINT NOT NULL, INDEX IDX_E9CDA30C1844E6B7 (server_id), INDEX IDX_E9CDA30CFE54D947 (group_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ts_group (id INT AUTO_INCREMENT NOT NULL, server_id INT DEFAULT NULL, server_group_id SMALLINT NOT NULL, server_group_name VARCHAR(128) NOT NULL, INDEX IDX_C682834F1844E6B7 (server_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE ts_criterion ADD CONSTRAINT FK_E9CDA30C1844E6B7 FOREIGN KEY (server_id) REFERENCES ts_sever (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE ts_criterion ADD CONSTRAINT FK_E9CDA30CFE54D947 FOREIGN KEY (group_id) REFERENCES ts_group (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE ts_group ADD CONSTRAINT FK_C682834F1844E6B7 FOREIGN KEY (server_id) REFERENCES ts_sever (id) ON DELETE CASCADE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE ts_criterion DROP FOREIGN KEY FK_E9CDA30C1844E6B7');
        $this->addSql('ALTER TABLE ts_group DROP FOREIGN KEY FK_C682834F1844E6B7');
        $this->addSql('ALTER TABLE ts_criterion DROP FOREIGN KEY FK_E9CDA30CFE54D947');
        $this->addSql('DROP TABLE ts_sever');
        $this->addSql('DROP TABLE ts_criterion');
        $this->addSql('DROP TABLE ts_group');
    }
}
