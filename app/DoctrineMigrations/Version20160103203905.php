<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160103203905 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE user_corporation ADD ceo_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user_corporation ADD CONSTRAINT FK_2D319F169435DF1D FOREIGN KEY (ceo_id) REFERENCES user (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_2D319F169435DF1D ON user_corporation (ceo_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE user_corporation DROP FOREIGN KEY FK_2D319F169435DF1D');
        $this->addSql('DROP INDEX UNIQ_2D319F169435DF1D ON user_corporation');
        $this->addSql('ALTER TABLE user_corporation DROP ceo_id');
    }
}
