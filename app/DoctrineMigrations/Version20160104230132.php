<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160104230132 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE ts_group_criterion (group_id INT NOT NULL, criterion_id INT NOT NULL, INDEX IDX_BFA4CBDDFE54D947 (group_id), INDEX IDX_BFA4CBDD97766307 (criterion_id), PRIMARY KEY(group_id, criterion_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE ts_group_criterion ADD CONSTRAINT FK_BFA4CBDDFE54D947 FOREIGN KEY (group_id) REFERENCES ts_group (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE ts_group_criterion ADD CONSTRAINT FK_BFA4CBDD97766307 FOREIGN KEY (criterion_id) REFERENCES ts_criterion (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE ts_criterion DROP FOREIGN KEY FK_E9CDA30CFE54D947');
        $this->addSql('DROP INDEX IDX_E9CDA30CFE54D947 ON ts_criterion');
        $this->addSql('ALTER TABLE ts_criterion DROP group_id');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE ts_group_criterion');
        $this->addSql('ALTER TABLE ts_criterion ADD group_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE ts_criterion ADD CONSTRAINT FK_E9CDA30CFE54D947 FOREIGN KEY (group_id) REFERENCES ts_group (id) ON DELETE CASCADE');
        $this->addSql('CREATE INDEX IDX_E9CDA30CFE54D947 ON ts_criterion (group_id)');
    }
}
