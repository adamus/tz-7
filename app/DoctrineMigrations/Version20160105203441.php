<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160105203441 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE ts_criterion ADD slug VARCHAR(128) NOT NULL, CHANGE comparison_type comparison_type VARCHAR(16) NOT NULL, CHANGE match_type match_type VARCHAR(16) NOT NULL');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_E9CDA30C989D9B62 ON ts_criterion (slug)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP INDEX UNIQ_E9CDA30C989D9B62 ON ts_criterion');
        $this->addSql('ALTER TABLE ts_criterion DROP slug, CHANGE comparison_type comparison_type SMALLINT NOT NULL, CHANGE match_type match_type SMALLINT NOT NULL');
    }
}
