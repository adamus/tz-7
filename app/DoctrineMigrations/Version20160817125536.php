<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160817125536 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE ts_group_criterion DROP FOREIGN KEY FK_BFA4CBDD97766307');
        $this->addSql('CREATE TABLE tz7_criterion (id INT AUTO_INCREMENT NOT NULL, slug VARCHAR(128) NOT NULL, comparison_type VARCHAR(16) NOT NULL, condition_value VARCHAR(255) NOT NULL, match_type VARCHAR(16) NOT NULL, UNIQUE INDEX UNIQ_68818C80989D9B62 (slug), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tz7_criteria (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(128) NOT NULL, enabled TINYINT(1) DEFAULT \'0\' NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tz7_criteria_has_criterion (criteria_id INT NOT NULL, criterion_id INT NOT NULL, INDEX IDX_3775A424990BEA15 (criteria_id), INDEX IDX_3775A42497766307 (criterion_id), PRIMARY KEY(criteria_id, criterion_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tz7_service (id INT AUTO_INCREMENT NOT NULL, alias VARCHAR(64) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE tz7_criteria_has_criterion ADD CONSTRAINT FK_3775A424990BEA15 FOREIGN KEY (criteria_id) REFERENCES tz7_criteria (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE tz7_criteria_has_criterion ADD CONSTRAINT FK_3775A42497766307 FOREIGN KEY (criterion_id) REFERENCES tz7_criterion (id) ON DELETE CASCADE');

        $this->addSql('INSERT INTO tz7_criterion (id, slug, comparison_type, condition_value, match_type) SELECT id, slug, comparison_type, condition_value, match_type FROM ts_criterion');
        $this->addSql('INSERT INTO tz7_criteria (id, name, enabled) SELECT id, server_group_name, 1 FROM ts_group WHERE managed_group = 1');
        $this->addSql('INSERT INTO tz7_criteria_has_criterion (criteria_id, criterion_id) SELECT tgc.group_id, tgc.criterion_id FROM ts_group_criterion tgc JOIN tz7_criteria tc ON tc.id = tgc.group_id');

        $this->addSql('DROP TABLE ts_criterion');
        $this->addSql('DROP TABLE ts_group_criterion');
        $this->addSql('ALTER TABLE ts_sever ADD service_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE ts_sever ADD CONSTRAINT FK_291736A9ED5CA9E6 FOREIGN KEY (service_id) REFERENCES tz7_service (id) ON DELETE CASCADE');

        $this->addSql('CREATE INDEX IDX_291736A9ED5CA9E6 ON ts_sever (service_id)');
        $this->addSql('ALTER TABLE ts_group ADD criteria_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE ts_group ADD CONSTRAINT FK_C682834F990BEA15 FOREIGN KEY (criteria_id) REFERENCES tz7_criteria (id) ON DELETE SET NULL');
        $this->addSql('CREATE INDEX IDX_C682834F990BEA15 ON ts_group (criteria_id)');

        $this->addSql('UPDATE ts_group SET criteria_id = id WHERE id IN (SELECT id FROM tz7_criteria)');

        $this->addSql('INSERT INTO tz7_service (id, alias) SELECT id, alias FROM ts_sever');
        $this->addSql('UPDATE ts_sever SET service_id = id');
        $this->addSql('ALTER TABLE ts_sever RENAME ts_server');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE tz7_criteria_has_criterion DROP FOREIGN KEY FK_3775A42497766307');
        $this->addSql('ALTER TABLE tz7_criteria_has_criterion DROP FOREIGN KEY FK_3775A424990BEA15');
        $this->addSql('ALTER TABLE ts_group DROP FOREIGN KEY FK_C682834F990BEA15');
        $this->addSql('ALTER TABLE ts_sever DROP FOREIGN KEY FK_291736A9ED5CA9E6');
        $this->addSql('CREATE TABLE ts_criterion (id INT AUTO_INCREMENT NOT NULL, server_id INT DEFAULT NULL, comparison_type VARCHAR(16) NOT NULL COLLATE utf8_unicode_ci, condition_value VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, match_type VARCHAR(16) NOT NULL COLLATE utf8_unicode_ci, slug VARCHAR(128) NOT NULL COLLATE utf8_unicode_ci, UNIQUE INDEX UNIQ_E9CDA30C989D9B62 (slug), INDEX IDX_E9CDA30C1844E6B7 (server_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ts_group_criterion (group_id INT NOT NULL, criterion_id INT NOT NULL, INDEX IDX_BFA4CBDDFE54D947 (group_id), INDEX IDX_BFA4CBDD97766307 (criterion_id), PRIMARY KEY(group_id, criterion_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE ts_criterion ADD CONSTRAINT FK_E9CDA30C1844E6B7 FOREIGN KEY (server_id) REFERENCES ts_sever (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE ts_group_criterion ADD CONSTRAINT FK_BFA4CBDD97766307 FOREIGN KEY (criterion_id) REFERENCES ts_criterion (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE ts_group_criterion ADD CONSTRAINT FK_BFA4CBDDFE54D947 FOREIGN KEY (group_id) REFERENCES ts_group (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE tz7_criterion');
        $this->addSql('DROP TABLE tz7_criteria');
        $this->addSql('DROP TABLE tz7_criteria_has_criterion');
        $this->addSql('DROP TABLE tz7_service');
        $this->addSql('DROP INDEX IDX_C682834F990BEA15 ON ts_group');
        $this->addSql('ALTER TABLE ts_group DROP criteria_id');
        $this->addSql('DROP INDEX IDX_291736A9ED5CA9E6 ON ts_sever');
        $this->addSql('ALTER TABLE ts_sever DROP service_id');
    }
}
