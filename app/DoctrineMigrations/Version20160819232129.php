<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160819232129 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE tz7_discord_bot (id INT AUTO_INCREMENT NOT NULL, service_id INT DEFAULT NULL, server_name VARCHAR(64) NOT NULL, bot_name VARCHAR(64) NOT NULL, invite_link VARCHAR(255) NOT NULL, token VARCHAR(255) NOT NULL, INDEX IDX_376DEA08ED5CA9E6 (service_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tz7_discord_bot_service (id INT AUTO_INCREMENT NOT NULL, bot_id INT DEFAULT NULL, handler_type VARCHAR(64) NOT NULL, channel INT DEFAULT NULL, is_active TINYINT(1) DEFAULT \'0\' NOT NULL, INDEX IDX_468BE15192C1C487 (bot_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tz7_discord_group (id INT AUTO_INCREMENT NOT NULL, server_id INT DEFAULT NULL, criteria_id INT DEFAULT NULL, name VARCHAR(64) NOT NULL, managed_group TINYINT(1) DEFAULT \'0\' NOT NULL, INDEX IDX_B17214D01844E6B7 (server_id), INDEX IDX_B17214D0990BEA15 (criteria_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tz7_discord_character_auth (id INT AUTO_INCREMENT NOT NULL, bot_id INT DEFAULT NULL, character_id INT DEFAULT NULL, discord_client_id BIGINT DEFAULT NULL, auth_token VARCHAR(32) NOT NULL, created_at DATETIME DEFAULT \'2000-01-01 00:00:00\' NOT NULL, INDEX IDX_8E45D2E492C1C487 (bot_id), INDEX IDX_8E45D2E41136BE75 (character_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE tz7_discord_bot ADD CONSTRAINT FK_376DEA08ED5CA9E6 FOREIGN KEY (service_id) REFERENCES tz7_service (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE tz7_discord_bot_service ADD CONSTRAINT FK_468BE15192C1C487 FOREIGN KEY (bot_id) REFERENCES tz7_discord_bot (id)');
        $this->addSql('ALTER TABLE tz7_discord_group ADD CONSTRAINT FK_B17214D01844E6B7 FOREIGN KEY (server_id) REFERENCES tz7_discord_bot (id)');
        $this->addSql('ALTER TABLE tz7_discord_group ADD CONSTRAINT FK_B17214D0990BEA15 FOREIGN KEY (criteria_id) REFERENCES tz7_criteria (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE tz7_discord_character_auth ADD CONSTRAINT FK_8E45D2E492C1C487 FOREIGN KEY (bot_id) REFERENCES tz7_discord_bot (id)');
        $this->addSql('ALTER TABLE tz7_discord_character_auth ADD CONSTRAINT FK_8E45D2E41136BE75 FOREIGN KEY (character_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE ts_server DROP FOREIGN KEY FK_291736A9ED5CA9E6');
        $this->addSql('DROP INDEX idx_291736a9ed5ca9e6 ON ts_server');
        $this->addSql('CREATE INDEX IDX_57ABFD0FED5CA9E6 ON ts_server (service_id)');
        $this->addSql('ALTER TABLE ts_server ADD CONSTRAINT FK_291736A9ED5CA9E6 FOREIGN KEY (service_id) REFERENCES tz7_service (id) ON DELETE CASCADE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE tz7_discord_bot_service DROP FOREIGN KEY FK_468BE15192C1C487');
        $this->addSql('ALTER TABLE tz7_discord_group DROP FOREIGN KEY FK_B17214D01844E6B7');
        $this->addSql('ALTER TABLE tz7_discord_character_auth DROP FOREIGN KEY FK_8E45D2E492C1C487');
        $this->addSql('DROP TABLE tz7_discord_bot');
        $this->addSql('DROP TABLE tz7_discord_bot_service');
        $this->addSql('DROP TABLE tz7_discord_group');
        $this->addSql('DROP TABLE tz7_discord_character_auth');
        $this->addSql('ALTER TABLE ts_server DROP FOREIGN KEY FK_57ABFD0FED5CA9E6');
        $this->addSql('DROP INDEX idx_57abfd0fed5ca9e6 ON ts_server');
        $this->addSql('CREATE INDEX IDX_291736A9ED5CA9E6 ON ts_server (service_id)');
        $this->addSql('ALTER TABLE ts_server ADD CONSTRAINT FK_57ABFD0FED5CA9E6 FOREIGN KEY (service_id) REFERENCES tz7_service (id) ON DELETE CASCADE');
    }
}
