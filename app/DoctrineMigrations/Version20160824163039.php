<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160824163039 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE tz7_eve_api_key (id INT NOT NULL, code VARCHAR(255) NOT NULL, type VARCHAR(255) NOT NULL, access_mask BIGINT NOT NULL, expires_at DATETIME DEFAULT NULL, last_checked_at DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tz7_eve_api_journal (id INT AUTO_INCREMENT NOT NULL, api_key_id INT DEFAULT NULL, character_id INT DEFAULT NULL, corporation_id INT DEFAULT NULL, function_alias VARCHAR(32) NOT NULL, last_called_at DATETIME DEFAULT NULL, cached_until DATETIME DEFAULT NULL, is_success TINYINT(1) NOT NULL, INDEX IDX_B6A9C1838BE312B3 (api_key_id), INDEX IDX_B6A9C1831136BE75 (character_id), INDEX IDX_B6A9C183B2685369 (corporation_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tz7_eve_api_character_has_key (character_id INT NOT NULL, key_id INT NOT NULL, INDEX IDX_7C2D4FC21136BE75 (character_id), INDEX IDX_7C2D4FC2D145533 (key_id), PRIMARY KEY(character_id, key_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE tz7_eve_api_journal ADD CONSTRAINT FK_B6A9C1838BE312B3 FOREIGN KEY (api_key_id) REFERENCES tz7_eve_api_key (id)');
        $this->addSql('ALTER TABLE tz7_eve_api_journal ADD CONSTRAINT FK_B6A9C1831136BE75 FOREIGN KEY (character_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE tz7_eve_api_journal ADD CONSTRAINT FK_B6A9C183B2685369 FOREIGN KEY (corporation_id) REFERENCES user_corporation (id)');
        $this->addSql('ALTER TABLE tz7_eve_api_character_has_key ADD CONSTRAINT FK_7C2D4FC21136BE75 FOREIGN KEY (character_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE tz7_eve_api_character_has_key ADD CONSTRAINT FK_7C2D4FC2D145533 FOREIGN KEY (key_id) REFERENCES tz7_eve_api_key (id) ON DELETE CASCADE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE tz7_eve_api_journal DROP FOREIGN KEY FK_B6A9C1838BE312B3');
        $this->addSql('ALTER TABLE tz7_eve_api_character_has_key DROP FOREIGN KEY FK_7C2D4FC2D145533');
        $this->addSql('DROP TABLE tz7_eve_api_key');
        $this->addSql('DROP TABLE tz7_eve_api_journal');
        $this->addSql('DROP TABLE tz7_eve_api_character_has_key');
    }
}
