<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160826183448 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE tz7_eve_api_function (id INT AUTO_INCREMENT NOT NULL, type VARCHAR(32) NOT NULL, name VARCHAR(32) NOT NULL, is_mandatory TINYINT(1) NOT NULL, access_mask BIGINT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tz7_eve_api_function_usage (id INT AUTO_INCREMENT NOT NULL, api_key_id INT DEFAULT NULL, function_id INT DEFAULT NULL, character_id INT DEFAULT NULL, corporation_id INT DEFAULT NULL, last_called_at DATETIME DEFAULT NULL, cached_until DATETIME DEFAULT NULL, is_enabled TINYINT(1) NOT NULL, is_success TINYINT(1) NOT NULL, INDEX IDX_38149DA48BE312B3 (api_key_id), INDEX IDX_38149DA467048801 (function_id), INDEX IDX_38149DA41136BE75 (character_id), INDEX IDX_38149DA4B2685369 (corporation_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE tz7_eve_api_function_usage ADD CONSTRAINT FK_38149DA48BE312B3 FOREIGN KEY (api_key_id) REFERENCES tz7_eve_api_key (id)');
        $this->addSql('ALTER TABLE tz7_eve_api_function_usage ADD CONSTRAINT FK_38149DA467048801 FOREIGN KEY (function_id) REFERENCES tz7_eve_api_function (id)');
        $this->addSql('ALTER TABLE tz7_eve_api_function_usage ADD CONSTRAINT FK_38149DA41136BE75 FOREIGN KEY (character_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE tz7_eve_api_function_usage ADD CONSTRAINT FK_38149DA4B2685369 FOREIGN KEY (corporation_id) REFERENCES user_corporation (id)');
        $this->addSql('DROP TABLE tz7_eve_api_journal');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE tz7_eve_api_function_usage DROP FOREIGN KEY FK_38149DA467048801');
        $this->addSql('CREATE TABLE tz7_eve_api_journal (id INT AUTO_INCREMENT NOT NULL, character_id INT DEFAULT NULL, api_key_id INT DEFAULT NULL, corporation_id INT DEFAULT NULL, function_alias VARCHAR(32) NOT NULL COLLATE utf8_unicode_ci, last_called_at DATETIME DEFAULT NULL, cached_until DATETIME DEFAULT NULL, is_success TINYINT(1) NOT NULL, INDEX IDX_B6A9C1838BE312B3 (api_key_id), INDEX IDX_B6A9C1831136BE75 (character_id), INDEX IDX_B6A9C183B2685369 (corporation_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE tz7_eve_api_journal ADD CONSTRAINT FK_B6A9C1831136BE75 FOREIGN KEY (character_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE tz7_eve_api_journal ADD CONSTRAINT FK_B6A9C1838BE312B3 FOREIGN KEY (api_key_id) REFERENCES tz7_eve_api_key (id)');
        $this->addSql('ALTER TABLE tz7_eve_api_journal ADD CONSTRAINT FK_B6A9C183B2685369 FOREIGN KEY (corporation_id) REFERENCES user_corporation (id)');
        $this->addSql('DROP TABLE tz7_eve_api_function');
        $this->addSql('DROP TABLE tz7_eve_api_function_usage');
    }
}
