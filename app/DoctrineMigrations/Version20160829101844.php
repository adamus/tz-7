<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160829101844 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE tz7_eve_api_function_usage DROP FOREIGN KEY FK_38149DA41136BE75');
        $this->addSql('ALTER TABLE tz7_eve_api_function_usage DROP FOREIGN KEY FK_38149DA467048801');
        $this->addSql('ALTER TABLE tz7_eve_api_function_usage DROP FOREIGN KEY FK_38149DA48BE312B3');
        $this->addSql('ALTER TABLE tz7_eve_api_function_usage DROP FOREIGN KEY FK_38149DA4B2685369');
        $this->addSql('ALTER TABLE tz7_eve_api_function_usage ADD CONSTRAINT FK_38149DA41136BE75 FOREIGN KEY (character_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE tz7_eve_api_function_usage ADD CONSTRAINT FK_38149DA467048801 FOREIGN KEY (function_id) REFERENCES tz7_eve_api_function (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE tz7_eve_api_function_usage ADD CONSTRAINT FK_38149DA48BE312B3 FOREIGN KEY (api_key_id) REFERENCES tz7_eve_api_key (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE tz7_eve_api_function_usage ADD CONSTRAINT FK_38149DA4B2685369 FOREIGN KEY (corporation_id) REFERENCES user_corporation (id) ON DELETE CASCADE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE tz7_eve_api_function_usage DROP FOREIGN KEY FK_38149DA48BE312B3');
        $this->addSql('ALTER TABLE tz7_eve_api_function_usage DROP FOREIGN KEY FK_38149DA467048801');
        $this->addSql('ALTER TABLE tz7_eve_api_function_usage DROP FOREIGN KEY FK_38149DA41136BE75');
        $this->addSql('ALTER TABLE tz7_eve_api_function_usage DROP FOREIGN KEY FK_38149DA4B2685369');
        $this->addSql('ALTER TABLE tz7_eve_api_function_usage ADD CONSTRAINT FK_38149DA48BE312B3 FOREIGN KEY (api_key_id) REFERENCES tz7_eve_api_key (id)');
        $this->addSql('ALTER TABLE tz7_eve_api_function_usage ADD CONSTRAINT FK_38149DA467048801 FOREIGN KEY (function_id) REFERENCES tz7_eve_api_function (id)');
        $this->addSql('ALTER TABLE tz7_eve_api_function_usage ADD CONSTRAINT FK_38149DA41136BE75 FOREIGN KEY (character_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE tz7_eve_api_function_usage ADD CONSTRAINT FK_38149DA4B2685369 FOREIGN KEY (corporation_id) REFERENCES user_corporation (id)');
    }
}
