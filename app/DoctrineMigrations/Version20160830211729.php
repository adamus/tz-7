<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160830211729 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE tz7_discord_bot_service ADD api_function_usage_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE tz7_discord_bot_service ADD CONSTRAINT FK_468BE151A199227A FOREIGN KEY (api_function_usage_id) REFERENCES tz7_eve_api_function_usage (id)');
        $this->addSql('CREATE INDEX IDX_468BE151A199227A ON tz7_discord_bot_service (api_function_usage_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE tz7_discord_bot_service DROP FOREIGN KEY FK_468BE151A199227A');
        $this->addSql('DROP INDEX IDX_468BE151A199227A ON tz7_discord_bot_service');
        $this->addSql('ALTER TABLE tz7_discord_bot_service DROP api_function_usage_id');
    }
}
