<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160917215111 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE tz7_discord_bot_channel (id INT AUTO_INCREMENT NOT NULL, bot_service_id INT DEFAULT NULL, name VARCHAR(32) NOT NULL, channel_id BIGINT DEFAULT NULL, type VARCHAR(16) NOT NULL, INDEX IDX_5EFF5C457576FD1 (bot_service_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE tz7_discord_bot_channel ADD CONSTRAINT FK_5EFF5C457576FD1 FOREIGN KEY (bot_service_id) REFERENCES tz7_discord_bot_service (id) ON DELETE CASCADE');

        $this->addSql("INSERT INTO tz7_discord_bot_channel (bot_service_id, name, channel_id, type) SELECT id, '#channel', channel, 'corporation' FROM tz7_discord_bot_service WHERE channel IS NOT NULL");

        $this->addSql('ALTER TABLE tz7_discord_bot_service DROP channel');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE tz7_discord_bot_channel');
        $this->addSql('ALTER TABLE tz7_discord_bot_service ADD channel BIGINT DEFAULT NULL');
    }
}
