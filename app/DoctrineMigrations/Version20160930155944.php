<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160930155944 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE tz7_eve_api_mail (id BIGINT NOT NULL, to_corporation_id INT DEFAULT NULL, to_alliance_id INT DEFAULT NULL, to_mailing_list_id BIGINT DEFAULT NULL, title VARCHAR(128) NOT NULL, body LONGTEXT DEFAULT NULL, INDEX IDX_4FEED8A8D03AFFBD (to_corporation_id), INDEX IDX_4FEED8A83E33D45C (to_alliance_id), INDEX IDX_4FEED8A8ADA2B785 (to_mailing_list_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tz7_eve_api_mail_recipient_character (mail_id BIGINT NOT NULL, character_id INT NOT NULL, INDEX IDX_2E0FB97C8776F01 (mail_id), INDEX IDX_2E0FB971136BE75 (character_id), PRIMARY KEY(mail_id, character_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tz7_eve_api_mailing_list (id BIGINT NOT NULL, display_name VARCHAR(128) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tz7_eve_api_mailing_list_member (mailing_list_id BIGINT NOT NULL, character_id INT NOT NULL, INDEX IDX_9367F8902C7EF3E4 (mailing_list_id), INDEX IDX_9367F8901136BE75 (character_id), PRIMARY KEY(mailing_list_id, character_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE tz7_eve_api_mail ADD CONSTRAINT FK_4FEED8A8D03AFFBD FOREIGN KEY (to_corporation_id) REFERENCES user_corporation (id)');
        $this->addSql('ALTER TABLE tz7_eve_api_mail ADD CONSTRAINT FK_4FEED8A83E33D45C FOREIGN KEY (to_alliance_id) REFERENCES user_alliance (id)');
        $this->addSql('ALTER TABLE tz7_eve_api_mail ADD CONSTRAINT FK_4FEED8A8ADA2B785 FOREIGN KEY (to_mailing_list_id) REFERENCES tz7_eve_api_mailing_list (id)');
        $this->addSql('ALTER TABLE tz7_eve_api_mail_recipient_character ADD CONSTRAINT FK_2E0FB97C8776F01 FOREIGN KEY (mail_id) REFERENCES tz7_eve_api_mail (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE tz7_eve_api_mail_recipient_character ADD CONSTRAINT FK_2E0FB971136BE75 FOREIGN KEY (character_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE tz7_eve_api_mailing_list_member ADD CONSTRAINT FK_9367F8902C7EF3E4 FOREIGN KEY (mailing_list_id) REFERENCES tz7_eve_api_mailing_list (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE tz7_eve_api_mailing_list_member ADD CONSTRAINT FK_9367F8901136BE75 FOREIGN KEY (character_id) REFERENCES user (id) ON DELETE CASCADE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE tz7_eve_api_mail_recipient_character DROP FOREIGN KEY FK_2E0FB97C8776F01');
        $this->addSql('ALTER TABLE tz7_eve_api_mail DROP FOREIGN KEY FK_4FEED8A8ADA2B785');
        $this->addSql('ALTER TABLE tz7_eve_api_mailing_list_member DROP FOREIGN KEY FK_9367F8902C7EF3E4');
        $this->addSql('DROP TABLE tz7_eve_api_mail');
        $this->addSql('DROP TABLE tz7_eve_api_mail_recipient_character');
        $this->addSql('DROP TABLE tz7_eve_api_mailing_list');
        $this->addSql('DROP TABLE tz7_eve_api_mailing_list_member');
    }
}
