<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160930162735 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE tz7_eve_api_mail ADD sender_id INT DEFAULT NULL AFTER id, ADD sent_date DATETIME NOT NULL AFTER sender_id');
        $this->addSql('ALTER TABLE tz7_eve_api_mail ADD CONSTRAINT FK_4FEED8A8F624B39D FOREIGN KEY (sender_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_4FEED8A8F624B39D ON tz7_eve_api_mail (sender_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE tz7_eve_api_mail DROP FOREIGN KEY FK_4FEED8A8F624B39D');
        $this->addSql('DROP INDEX IDX_4FEED8A8F624B39D ON tz7_eve_api_mail');
        $this->addSql('ALTER TABLE tz7_eve_api_mail DROP sender_id, DROP sent_date');
    }
}
