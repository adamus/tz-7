<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161012222027 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE tz7_discord_bot_service_has_channel (bot_service_id INT NOT NULL, bot_channel_id INT NOT NULL, INDEX IDX_B632E25A57576FD1 (bot_service_id), INDEX IDX_B632E25AC8FE679D (bot_channel_id), PRIMARY KEY(bot_service_id, bot_channel_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE tz7_discord_bot_service_has_channel ADD CONSTRAINT FK_B632E25A57576FD1 FOREIGN KEY (bot_service_id) REFERENCES tz7_discord_bot_service (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE tz7_discord_bot_service_has_channel ADD CONSTRAINT FK_B632E25AC8FE679D FOREIGN KEY (bot_channel_id) REFERENCES tz7_discord_bot_channel (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE tz7_discord_bot_channel DROP FOREIGN KEY FK_5EFF5C457576FD1');

        $this->addSql('INSERT INTO tz7_discord_bot_service_has_channel (bot_service_id, bot_channel_id) SELECT bot_service_id, id FROM tz7_discord_bot_channel');

        $this->addSql('DROP INDEX IDX_5EFF5C457576FD1 ON tz7_discord_bot_channel');
        $this->addSql('ALTER TABLE tz7_discord_bot_channel ADD types LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', DROP bot_service_id, DROP type');

        $this->addSql('UPDATE tz7_discord_bot_channel SET types = \'a:0:{}\'');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE tz7_discord_bot_service_has_channel');
        $this->addSql('ALTER TABLE tz7_discord_bot_channel ADD bot_service_id INT DEFAULT NULL, ADD type VARCHAR(32) NOT NULL COLLATE utf8_unicode_ci, DROP types');
        $this->addSql('ALTER TABLE tz7_discord_bot_channel ADD CONSTRAINT FK_5EFF5C457576FD1 FOREIGN KEY (bot_service_id) REFERENCES tz7_discord_bot_service (id) ON DELETE CASCADE');
        $this->addSql('CREATE INDEX IDX_5EFF5C457576FD1 ON tz7_discord_bot_channel (bot_service_id)');
    }
}
