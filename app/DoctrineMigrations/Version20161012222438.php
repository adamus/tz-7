<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161012222438 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE tz7_discord_bot_service_has_channel DROP FOREIGN KEY FK_B632E25A57576FD1');
        $this->addSql('ALTER TABLE tz7_discord_bot_service_has_channel DROP FOREIGN KEY FK_B632E25AC8FE679D');
        $this->addSql('DROP INDEX idx_b632e25a57576fd1 ON tz7_discord_bot_service_has_channel');
        $this->addSql('CREATE INDEX IDX_69BDB61D57576FD1 ON tz7_discord_bot_service_has_channel (bot_service_id)');
        $this->addSql('DROP INDEX idx_b632e25ac8fe679d ON tz7_discord_bot_service_has_channel');
        $this->addSql('CREATE INDEX IDX_69BDB61DC8FE679D ON tz7_discord_bot_service_has_channel (bot_channel_id)');
        $this->addSql('ALTER TABLE tz7_discord_bot_service_has_channel ADD CONSTRAINT FK_B632E25A57576FD1 FOREIGN KEY (bot_service_id) REFERENCES tz7_discord_bot_service (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE tz7_discord_bot_service_has_channel ADD CONSTRAINT FK_B632E25AC8FE679D FOREIGN KEY (bot_channel_id) REFERENCES tz7_discord_bot_channel (id) ON DELETE CASCADE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE tz7_discord_bot_service_has_channel DROP FOREIGN KEY FK_69BDB61D57576FD1');
        $this->addSql('ALTER TABLE tz7_discord_bot_service_has_channel DROP FOREIGN KEY FK_69BDB61DC8FE679D');
        $this->addSql('DROP INDEX idx_69bdb61d57576fd1 ON tz7_discord_bot_service_has_channel');
        $this->addSql('CREATE INDEX IDX_B632E25A57576FD1 ON tz7_discord_bot_service_has_channel (bot_service_id)');
        $this->addSql('DROP INDEX idx_69bdb61dc8fe679d ON tz7_discord_bot_service_has_channel');
        $this->addSql('CREATE INDEX IDX_B632E25AC8FE679D ON tz7_discord_bot_service_has_channel (bot_channel_id)');
        $this->addSql('ALTER TABLE tz7_discord_bot_service_has_channel ADD CONSTRAINT FK_69BDB61D57576FD1 FOREIGN KEY (bot_service_id) REFERENCES tz7_discord_bot_service (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE tz7_discord_bot_service_has_channel ADD CONSTRAINT FK_69BDB61DC8FE679D FOREIGN KEY (bot_channel_id) REFERENCES tz7_discord_bot_channel (id) ON DELETE CASCADE');
    }
}
