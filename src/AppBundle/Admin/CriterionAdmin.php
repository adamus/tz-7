<?php


namespace AppBundle\Admin;


use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

use AppBundle\Entity\Criterion;


class CriterionAdmin extends Admin
{
    protected $comparisonTypes = [
        Criterion::COMPARE_CHARACTER_ID => 'compare.character_id',
        Criterion::COMPARE_CHARACTER_NAME => 'compare.character_name',
        Criterion::COMPARE_CORPORATION_ID => 'compare.corporation_id',
        Criterion::COMPARE_CORPORATION_NAME => 'compare.corporation_name',
        Criterion::COMPARE_ALLIANCE_ID => 'compare.alliance_id',
        Criterion::COMPARE_ALLIANCE_NAME => 'compare.alliance_name',
        Criterion::COMPARE_NICK_NAME => 'compare.nick_name'
    ];

    protected $matchTypes = [
        Criterion::MATCH_EQUAL => 'match.equal',
        Criterion::NEGATE_EQUAL => 'negate.equal',
        Criterion::MATCH_PATTERN => 'match.pattern',
        Criterion::NEGATE_PATTERN => 'negate.pattern',
        Criterion::MATCH_EXPRESSION => 'match.expression'
    ];


    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('comparisonType', 'choice', array('choices' => $this->comparisonTypes))
            ->add('condition')
            ->add('matchType', 'choice', array('choices' => $this->matchTypes))
        ;
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('comparisonType', null, array(), 'choice', array('choices' => $this->comparisonTypes))
            ->add('condition')
            ->add('slug')
            ->add('matchType', null, array(), 'choice', array('choices' => $this->matchTypes))
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->add('comparisonType', 'choice', array('choices' => $this->comparisonTypes))
            ->add('condition')
            ->add('matchType', 'choice', array('choices' => $this->matchTypes))
        ;
    }

}