<?php


namespace AppBundle\Admin;


use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Tz7\EveDiscordBundle\Model\DiscordBotChannelInterface;


class DiscordBotChannelAdmin extends Admin
{
    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('name')
            ->add('channelId', 'text')
            ->add('types', 'choice', [
                'choices' => [
                    DiscordBotChannelInterface::TYPE_ALL => 'Debug',
                    DiscordBotChannelInterface::TYPE_PRIVATE => 'Character / Mail List',
                    DiscordBotChannelInterface::TYPE_CORPORATION_MEMBER => 'Corporation member',
                    DiscordBotChannelInterface::TYPE_CORPORATION_DIRECTOR => 'Corporation director',
                    DiscordBotChannelInterface::TYPE_ALLIANCE_MEMBER => 'Alliance member',
                    DiscordBotChannelInterface::TYPE_ALLIANCE_DIRECTOR => 'Alliance director'
                ],
                'multiple' => true,
                'expanded' => true
            ])
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->add('name')
            ->add('botServices')
            ->add('types')
        ;
    }
}