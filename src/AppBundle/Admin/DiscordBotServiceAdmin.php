<?php


namespace AppBundle\Admin;


use AppBundle\Entity\DiscordBotChannel;
use Doctrine\ORM\EntityRepository;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Tz7\EveApiBundle\Model\Api\ApiFunctionInterface;
use Tz7\EveDiscordBundle\Service\BotServices;

class DiscordBotServiceAdmin extends Admin
{
    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $timers = [
            60 => 'Every minute',
            300 => 'Every 5 minutes',
            600 => 'Every 10 minutes',
            1800 => 'Every 30 minutes',
            3600 => 'Hourly'
        ];

        $formMapper
            ->add('bot', 'sonata_type_model_list')
            ->add('botChannels', 'entity', [
                'class' => DiscordBotChannel::class,
                'required' => false,
                'multiple' => true,
                'expanded' => true
            ])
            ->add('handlerType', 'choice', ['choices' => $this->getHandlerTypes()])
            ->add('apiFunctionUsage', null, [
                'query_builder' => function(EntityRepository $repo) {
                    return $repo->createQueryBuilder('u')
                        ->join('u.function', 'f')
                        ->where('f.type IN (:types)')
                        ->andWhere('u.enabled = true')
                        ->andWhere('u.success = true')
                        ->setParameter('types', [
                            ApiFunctionInterface::TYPE_CHARACTER,
                            ApiFunctionInterface::TYPE_CORPORATION
                        ]);
                }
            ])
            ->add('tickPeriod', 'choice', ['choices' => $timers])
            ->add('active', 'checkbox', ['required' => false])
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->add('bot')
            ->add('handlerType')
            ->add('tickPeriod')
            ->add('active', 'boolean', ['editable' => true])
        ;
    }

    /**
     * @return array
     */
    protected function getHandlerTypes()
    {
        /** @var BotServices $botServices */
        $botServices = $this->getConfigurationPool()->getContainer()->get('tz7.eve_discord.bot_services');

        $types = [];
        foreach ($botServices->getServiceHandlerTypes() as $id) {
            $types[$id] = $id;
        }
        return $types;
    }
}