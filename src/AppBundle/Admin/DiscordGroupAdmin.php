<?php


namespace AppBundle\Admin;


use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class DiscordGroupAdmin extends Admin
{
    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('server', 'sonata_type_model_list')
            ->add('name')
            ->add('managedGroup', 'checkbox', ['required' => false])
            ->add('criteria', 'sonata_type_model_list')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->add('server')
            ->add('name')
            ->add('managedGroup', 'boolean', ['editable' => true])
        ;
    }

}