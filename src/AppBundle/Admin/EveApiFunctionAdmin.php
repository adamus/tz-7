<?php


namespace AppBundle\Admin;


use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Tz7\EveApiBundle\Model\Api\ApiFunctionInterface;


class EveApiFunctionAdmin extends Admin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('type', 'text', ['disabled' => true]) // @TODO const choices
            ->add('name', 'text', ['disabled' => true]) // @TODO const choices
            ->add('updatePeriod')
            ->add('mandatory', 'checkbox', ['required' => false])
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->add('type')
            ->add('name')
            ->add('updatePeriod')
            ->add('mandatory')
        ;
    }
}