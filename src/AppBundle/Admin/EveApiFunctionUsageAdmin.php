<?php


namespace AppBundle\Admin;


use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

use AppBundle\Entity\Criterion;


class EveApiFunctionUsageAdmin extends Admin
{
    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('apiKey')
            ->add('function')
            ->add('character')
            ->add('corporation')
            ->add('lastCalledAt', 'sonata_type_datetime_picker', ['required' => false, 'widget' => 'single_text', 'format' => 'yyyy-MM-dd HH:mm:ss'])
            ->add('cachedUntil', 'sonata_type_datetime_picker', ['required' => false, 'widget' => 'single_text', 'format' => 'yyyy-MM-dd HH:mm:ss'])
            ->add('enabled', 'checkbox', ['required' => false])
        ;
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('apiKey')
            ->add('character')
            ->add('corporation')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->add('apiKey')
            ->add('function')
            ->add('character')
            ->add('corporation')
            ->add('lastCalledAt')
            ->add('cachedUntil')
            ->add('enabled')
        ;
    }

}