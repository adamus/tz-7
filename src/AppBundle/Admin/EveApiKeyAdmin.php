<?php


namespace AppBundle\Admin;


use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;


class EveApiKeyAdmin extends Admin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('id', 'text')
            ->add('code', 'text')
            ->add(
                'functionUsages',
                'sonata_type_collection',
                [
                    'required' => false,
                    'by_reference' => false,
                    'label' => false
                ],
                [
                    'edit' => 'inline',
                    'inline' => 'table'
                ]
            )
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->add('type')
            ->add('accessMask')
            ->add('expiresAt')
            ->add('lastCheckedAt')
        ;
    }

}