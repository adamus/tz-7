<?php


namespace AppBundle\Admin;


use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class TeamSpeakGroupAdmin extends Admin
{
    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Server')
                ->add('server', 'sonata_type_model_list')
            ->end()
            ->with('Details')
                ->add('serverGroupId')
                ->add('serverGroupName')
                ->add('managedGroup', 'checkbox', ['required' => false])
            ->end()
            ->with('Criteria')
                ->add('criteria', 'sonata_type_model_list', ['label' => false])
            ->end()
        ;
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('serverGroupId')
            ->add('serverGroupName')
            ->add('managedGroup')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->add('server')
            ->add('serverGroupId')
            ->add('serverGroupName')
            ->add('managedGroup', 'boolean', ['editable' => true])
        ;
    }

}