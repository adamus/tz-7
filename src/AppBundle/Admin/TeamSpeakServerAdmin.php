<?php


namespace AppBundle\Admin;


use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class TeamSpeakServerAdmin extends Admin
{
    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Settings')
                ->add('alias')
            ->end()
            ->with('Connection')
                ->add('host')
                ->add('queryPort')
                ->add('clientPort')
            ->end()
            ->with('Credentials')
                ->add('username')
                ->add('password', 'password')
            ->end()
        ;
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('alias')
            ->add('host')
            ->add('queryPort')
            ->add('clientPort')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->addIdentifier('alias')
            ->add('host')
            ->add('queryPort')
            ->add('clientPort')
        ;
    }

}