<?php


namespace AppBundle\Command;


use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use AppBundle\Entity\TeamSpeakServer;
use AppBundle\TeamSpeak\TeamSpeakClientData;


class TeamSpeakCheckingCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('app:ts-check')
            ->setDescription('Check TS users')
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var EntityManager $em */
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');

        /** @var TeamSpeakClientData $tsClientData */
        $tsClientData = $this->getContainer()->get('ts_client_data');

        /** @var TeamSpeakServer[] $servers*/
        $servers = $em->createQueryBuilder()
            ->select('s')->from('AppBundle:TeamSpeakServer', 's')
            ->getQuery()->getResult()
        ;

        foreach ($servers as $server) {
            try {
                $tsClientData->checkClientsOfServer($server);
            } catch (\Exception $ex) {
                $this->getContainer()->get('logger')->error(sprintf('%s: "%s"', __METHOD__, $ex->getMessage()));
            }
        }

        return 0;
    }
}