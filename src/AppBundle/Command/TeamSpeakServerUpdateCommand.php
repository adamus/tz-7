<?php


namespace AppBundle\Command;


use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use AppBundle\Entity\TeamSpeakServer;
use AppBundle\TeamSpeak\TeamSpeakServerData;


class TeamSpeakServerUpdateCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('app:ts-update')
            ->setDescription('Update static data of TeamSpeak servers')
            ->addOption('ttl', null, InputOption::VALUE_OPTIONAL, "Server data TTL", "1 hour")
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var EntityManager $em */
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');

        /** @var TeamSpeakServerData $teamSpeakServer */
        $teamSpeakServer = $this->getContainer()->get('ts_server_data');

        $updateAge = (new \DateTime('now'))->modify(sprintf('-%s', $input->getOption('ttl')));

        /** @var TeamSpeakServer[] $servers*/
        $servers = $em->createQueryBuilder()
            ->select('s')->from('AppBundle:TeamSpeakServer', 's')
            ->where('s.updatedAt < :updateAge')->setParameter('updateAge', $updateAge)
            ->getQuery()->getResult()
        ;

        foreach ($servers as $server)
        {
            $teamSpeakServer->updateServerGroups($server);
            $server->setUpdatedAt(new \DateTime('now'));
            $em->flush();
        }

        return 0;
    }
}