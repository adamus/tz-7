<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration as FE;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    /**
     * @FE\Route("/", name="homepage")
     * @return mixed
     */
    public function indexAction()
    {
        return $this->render('default/index.html.twig');
    }

    /**
     * @FE\Route("/login", name="app_login")
     * @return Response
     */
    public function appLoginAction()
    {
        return $this->render('security/app_login.html.twig');
    }
}
