<?php

namespace AppBundle\Controller;


use Sensio\Bundle\FrameworkExtraBundle\Configuration as FE;
use Noxlogic\RateLimitBundle\Annotation as NX;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

use UserBundle\Entity\User;
use Tz7\EveDiscordBundle\Service\BotAccess;


/**
 * @FE\Route("/apps")
 */
class DiscordController extends Controller
{
    /**
     * @FE\Route("/discord", name="discord_index")
     * @return Response
     */
    public function indexAction()
    {
        /** @var User $user */
        $user = $this->getUser();

        /** @var BotAccess $botAccess */
        $botAccess = $this->get('tz7.eve_discord.bot_access');

        return $this->render('discord/index.html.twig', ['auth' => $botAccess->getBotAccessOfCharacter($user)]);
    }

    /**
     * @FE\Route("/discord/help", name="discord_help")
     * @return Response
     */
    public function helpAction()
    {
        return $this->render('discord/help.html.twig');
    }
}