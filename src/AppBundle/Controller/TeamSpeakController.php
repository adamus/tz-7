<?php


namespace AppBundle\Controller;


use Doctrine\ORM\EntityManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration as FE;
use Noxlogic\RateLimitBundle\Annotation as NX;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use TeamSpeak3\Node\Client;

use AppBundle\Entity\TeamSpeakServer;
use AppBundle\Entity\TeamSpeakClient;
use AppBundle\Repository\TeamSpeakClientRepository;
use AppBundle\TeamSpeak\TeamSpeakClientData;
use UserBundle\Entity\User;


/**
 * @FE\Route("/apps")
 */
class TeamSpeakController extends Controller
{
    /**
     * @FE\Route("/team-speak", name="ts_select_host", host="%team_speak_host%")
     * @FE\Route("/team-speak", name="ts_select")
     * @return Response
     */
    public function selectAction()
    {
        $servers = $this->entityManager()->getRepository('AppBundle:TeamSpeakServer')->findAll();

        if (count($servers) == 1) {
            /** @var TeamSpeakServer $server */
            $server = reset($servers);
            return $this->redirectToRoute('ts_server', ['server' => $server->getId()]);
        }

        return $this->render('team-speak/select.html.twig', ['servers' => $servers]);
    }

    /**
     * @FE\Route("/team-speak/my-clients", name="ts_clients")
     * @FE\Security("has_role('ROLE_USER')")
     * @param Request $request
     * @return Response
     */
    public function clientsAction(Request $request)
    {
        if ($request->isMethod('post'))
        {
            $clientId = $request->request->get('delete');
            /** @var User $user */
            $user = $this->getUser();
            $client = $user->getTeamSpeakClients()->filter(function(TeamSpeakClient $teamSpeakClient) use ($clientId) {
                return $clientId == $teamSpeakClient->getId();
            })->first();

            if ($client) {
                $user->removeTeamSpeakClient($client);
                $this->entityManager()->flush();
            }
        }

        return $this->render('team-speak/clients.html.twig');
    }

    /**
     * @FE\Route("/team-speak/{server}", name="ts_server")
     * @FE\ParamConverter("server", options={"mapping"={"server"="id"}})
     * @FE\Security("has_role('ROLE_USER')")
     * @param TeamSpeakServer $server
     * @return Response
     */
    public function serverAction(TeamSpeakServer $server)
    {
        /** @var User $user */
        $user = $this->getUser();
        $clientList = $this->tsClientData()->getClientsOfServer($server);
        $userMatches = $this->tsClientData()->matchClientForUser($user, $clientList);

        if (count($userMatches) === 1) {
            return $this->redirectToRoute('ts_pair', [
                'server' => $server->getId(),
                'cdbid' => (int)$userMatches[0]->getProperty('client_database_id')
            ]);
        }

        return $this->render('team-speak/server.html.twig', [
            'userMatches' => $userMatches,
            'server' => $server
        ]);
    }

    /**
     * @FE\Route("/team-speak/{server}/pair/{cdbid}", name="ts_pair")
     * @FE\ParamConverter("server", options={"mapping"={"server"="id"}})
     * @FE\Security("has_role('ROLE_USER')")
     * @NX\RateLimit(methods={"POST"}, limit="2", period="30")
     * @param Request $request
     * @param TeamSpeakServer $server
     * @param int $cdbid
     * @return Response
     */
    public function pairAction(Request $request, TeamSpeakServer $server, $cdbid)
    {
        /** @var User $user */
        $user = $this->getUser();
        $clientList = $this->tsClientData()->getClientsOfServer($server);
        $userMatches = $this->tsClientData()->matchClientForUser($user, $clientList);

        $selected = null;
        foreach ($userMatches as $client) {
            if (((int)$client->getProperty('client_database_id')) == $cdbid) {
                $selected = $client;
                break;
            }
        }

        if ($selected instanceof Client) {
            $uniqueId = (string)$selected->getProperty('client_unique_identifier');

            /** @var TeamSpeakClientRepository $clientRepo */
            $clientRepo = $this->getDoctrine()->getRepository(TeamSpeakClient::class);

            /** @var TeamSpeakClient $userClient */
            $userClient = $clientRepo->findUserClient($user, $uniqueId);

            if ($request->isMethod('POST')) {
                if (!$userClient)
                {
                    $userClient = new TeamSpeakClient();
                    $userClient->setUser($user);
                    $userClient->setUniqueId($uniqueId);
                    $user->addTeamSpeakClient($userClient);
                    try {
                        $this->entityManager()->persist($userClient);
                        $this->entityManager()->flush();
                    } catch (\Exception $ex) {
                        $this->get('logger')->error(sprintf('%s: "%s"', __METHOD__, $ex->getMessage()));
                    }
                }

                try {
                    $this->tsClientData()->checkUserClientGroups($userClient, $selected, $server);
                } catch (\Exception $ex) {
                    $this->get('logger')->error(sprintf('%s: "%s"', __METHOD__, $ex->getMessage()));
                }
            }

            return $this->render('team-speak/pair.html.twig', [
                'server' => $server,
                'selected' => $selected,
                'exists' => !!$userClient
            ]);
        } else {
            return $this->redirectToRoute('ts_select');
        }
    }

    /**
     * @return EntityManager
     */
    protected function entityManager()
    {
        return $this->getDoctrine()->getManager();
    }

    /**
     * @return TeamSpeakClientData
     */
    protected function tsClientData()
    {
        return $this->get('ts_client_data');
    }
}
