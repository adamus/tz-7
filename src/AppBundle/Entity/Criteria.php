<?php


namespace AppBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Tz7\EveCriterionBundle\Model\AbstractCriteria;


/**
 * @ORM\Table(name="tz7_criteria")
 * @ORM\Entity
 */
class Criteria extends AbstractCriteria
{
    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="TeamSpeakGroup", mappedBy="criteria", cascade={"persist"})
     */
    protected $teamSpeakGroups;

    /**
     *
     */
    public function __construct()
    {
        parent::__construct();
        $this->teamSpeakGroups = new ArrayCollection();
    }

    /**
     * Add teamSpeakGroup
     *
     * @param TeamSpeakGroup $teamSpeakGroup
     *
     * @return Criteria
     */
    public function addTeamSpeakGroup(TeamSpeakGroup $teamSpeakGroup)
    {
        $this->teamSpeakGroups[] = $teamSpeakGroup;

        return $this;
    }

    /**
     * Remove teamSpeakGroup
     *
     * @param TeamSpeakGroup $teamSpeakGroup
     */
    public function removeTeamSpeakGroup(TeamSpeakGroup $teamSpeakGroup)
    {
        $this->teamSpeakGroups->removeElement($teamSpeakGroup);
    }

    /**
     * Get teamSpeakGroups
     *
     * @return ArrayCollection
     */
    public function getTeamSpeakGroups()
    {
        return $this->teamSpeakGroups;
    }
}
