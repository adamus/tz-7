<?php


namespace AppBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use Tz7\EveCriterionBundle\Model\AbstractCriterion;


/**
 * @ORM\Table(name="tz7_criterion")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 */
class Criterion extends AbstractCriterion
{
    /**
     * @var string
     * @ORM\Column(name="slug", type="string", length=128, unique=true)
     */
    protected $slug;

    /**
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     * @return $this
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
        return $this;
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function generateSlug()
    {
        $this->slug = sprintf('%s %s "%s"', $this->comparisonType, $this->matchType, $this->condition);
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->slug;
    }
}