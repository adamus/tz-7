<?php


namespace AppBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use Tz7\EveDiscordBundle\Model\AbstractDiscordBot;


/**
 * @ORM\Table(name="tz7_discord_bot")
 * @ORM\Entity(repositoryClass="Tz7\EveDiscordBundle\Repository\DiscordBotRepository")
 */
class DiscordBot extends AbstractDiscordBot
{

}