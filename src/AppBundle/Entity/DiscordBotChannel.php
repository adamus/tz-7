<?php


namespace AppBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use Tz7\EveDiscordBundle\Model\AbstractDiscordBotChannel;


/**
 * @ORM\Table(name="tz7_discord_bot_channel")
 * @ORM\Entity
 */
class DiscordBotChannel extends AbstractDiscordBotChannel
{

}