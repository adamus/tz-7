<?php


namespace AppBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use Tz7\EveDiscordBundle\Model\AbstractDiscordBotService;


/**
 * @ORM\Table(name="tz7_discord_bot_service")
 * @ORM\Entity
 */
class DiscordBotService extends AbstractDiscordBotService
{

}