<?php


namespace AppBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use Tz7\EveDiscordBundle\Model\AbstractDiscordGroup;


/**
 * @ORM\Table(name="tz7_discord_group")
 * @ORM\Entity
 */
class DiscordGroup extends AbstractDiscordGroup
{

}