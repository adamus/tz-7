<?php


namespace AppBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use Tz7\EveApiBundle\Model\Api\AbstractApiFunction;


/**
 * @ORM\Entity
 * @ORM\Table(name="tz7_eve_api_function")
 */
class EveApiFunction extends AbstractApiFunction
{

}
