<?php


namespace AppBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use Tz7\EveApiBundle\Model\Api\AbstractApiFunctionUsage;


/**
 * @ORM\Entity(repositoryClass="Tz7\EveApiBundle\Repository\ApiFunctionUsageRepository")
 * @ORM\Table(name="tz7_eve_api_function_usage")
 */
class EveApiFunctionUsage extends AbstractApiFunctionUsage
{

}
