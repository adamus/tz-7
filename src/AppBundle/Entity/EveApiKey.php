<?php


namespace AppBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use Tz7\EveApiBundle\Model\Api\AbstractApiKey;


/**
 * @ORM\Entity
 * @ORM\Table(name="tz7_eve_api_key")
 */
class EveApiKey extends AbstractApiKey
{

}
