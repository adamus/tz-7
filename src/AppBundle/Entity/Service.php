<?php

namespace AppBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Tz7\EveDiscordBundle\Model\DiscordServiceProviderInterface;


/**
 * @ORM\Table(name="tz7_service")
 * @ORM\Entity
 */
class Service implements DiscordServiceProviderInterface
{
    /**
     * @var integer
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(name="alias", type="string", length=64)
     */
    protected $alias;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="TeamSpeakServer", mappedBy="service", cascade={"persist"})
     */
    protected $teamSpeakServers;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="DiscordBot", mappedBy="service", cascade={"persist"})
     */
    protected $discordBots;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->teamSpeakServers = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set alias
     *
     * @param string $alias
     *
     * @return Service
     */
    public function setAlias($alias)
    {
        $this->alias = $alias;

        return $this;
    }

    /**
     * Get alias
     *
     * @return string
     */
    public function getAlias()
    {
        return $this->alias;
    }

    /**
     * Add teamSpeakServer
     *
     * @param TeamSpeakServer $teamSpeakServer
     *
     * @return Service
     */
    public function addTeamSpeakServer(TeamSpeakServer $teamSpeakServer)
    {
        $teamSpeakServer->setService($this);
        $this->teamSpeakServers[] = $teamSpeakServer;

        return $this;
    }

    /**
     * Remove teamSpeakServer
     *
     * @param TeamSpeakServer $teamSpeakServer
     * @return $this
     */
    public function removeTeamSpeakServer(TeamSpeakServer $teamSpeakServer)
    {
        $this->teamSpeakServers->removeElement($teamSpeakServer);

        return $this;
    }

    /**
     * Get teamSpeakServers
     *
     * @return ArrayCollection
     */
    public function getTeamSpeakServers()
    {
        return $this->teamSpeakServers;
    }

    /**
     * Add discordBot
     *
     * @param DiscordBot $discordBot
     *
     * @return Service
     */
    public function addDiscordBot(DiscordBot $discordBot)
    {
        $this->discordBots[] = $discordBot;

        return $this;
    }

    /**
     * Remove discordBot
     *
     * @param DiscordBot $discordBot
     * @return $this
     */
    public function removeDiscordBot(DiscordBot $discordBot)
    {
        $this->discordBots->removeElement($discordBot);

        return $this;
    }

    /**
     * Get discordBots
     *
     * @return ArrayCollection
     */
    public function getDiscordBots()
    {
        return $this->discordBots;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string)$this->getAlias();
    }
}
