<?php

namespace AppBundle\Entity;


use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Table(name="ts_group")
 * @ORM\Entity
 */
class TeamSpeakGroup
{
    /**
     * @var integer
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var TeamSpeakServer
     * @ORM\ManyToOne(targetEntity="TeamSpeakServer", inversedBy="groups")
     * @ORM\JoinColumn(name="server_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $server;

    /**
     * @var integer
     * @ORM\Column(name="server_group_id", type="smallint")
     */
    protected $serverGroupId;

    /**
     * @var string
     * @ORM\Column(name="server_group_name", type="string", length=128)
     */
    protected $serverGroupName;

    /**
     * @var Criteria
     * @ORM\ManyToOne(targetEntity="Criteria", inversedBy="teamSpeakGroups", cascade={"persist"})
     * @ORM\JoinColumn(name="criteria_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $criteria;

    /**
     * @var boolean
     * @ORM\Column(name="managed_group", type="boolean", options={"default": "0"})
     */
    protected $managedGroup;

    public function __construct()
    {
        $this->managedGroup = false;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return TeamSpeakServer
     */
    public function getServer()
    {
        return $this->server;
    }

    /**
     * @param TeamSpeakServer $server
     *
     * @return $this
     */
    public function setServer($server)
    {
        $this->server = $server;

        return $this;
    }

    /**
     * @return int
     */
    public function getServerGroupId()
    {
        return $this->serverGroupId;
    }

    /**
     * @param int $serverGroupId
     *
     * @return $this
     */
    public function setServerGroupId($serverGroupId)
    {
        $this->serverGroupId = $serverGroupId;

        return $this;
    }

    /**
     * @return string
     */
    public function getServerGroupName()
    {
        return $this->serverGroupName;
    }

    /**
     * @param string $serverGroupName
     *
     * @return $this
     */
    public function setServerGroupName($serverGroupName)
    {
        $this->serverGroupName = $serverGroupName;

        return $this;
    }

    /**
     * Set criteria
     *
     * @param Criteria $criteria
     *
     * @return TeamSpeakGroup
     */
    public function setCriteria(Criteria $criteria = null)
    {
        $this->criteria = $criteria;

        return $this;
    }

    /**
     * Get criteria
     *
     * @return Criteria
     */
    public function getCriteria()
    {
        return $this->criteria;
    }

    /**
     * @return boolean
     */
    public function isManagedGroup()
    {
        return $this->managedGroup;
    }

    /**
     * @param boolean $managedGroup
     */
    public function setManagedGroup($managedGroup)
    {
        $this->managedGroup = $managedGroup;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        $string = "";

        if ($this->server)
        {
            $string .= $this->server->__toString() . " ";
        }

        $string .= sprintf('%s (%d)', $this->serverGroupName, $this->serverGroupId);

        return $string;
    }
}
