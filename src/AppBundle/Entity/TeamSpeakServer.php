<?php

namespace AppBundle\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="ts_server")
 * @ORM\Entity
 */
class TeamSpeakServer
{
    /**
     * @var integer
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var Service
     * @ORM\ManyToOne(targetEntity="Service", inversedBy="teamSpeakServers", cascade={"persist"})
     * @ORM\JoinColumn(name="service_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $service;

    /**
     * @var string
     * @ORM\Column(name="alias", type="string", length=64)
     */
    protected $alias;

    /**
     * @var string
     * @ORM\Column(name="host", type="string", length=64)
     */
    protected $host;

    /**
     * @var string
     * @ORM\Column(name="username", type="string", length=255)
     */
    protected $username;

    /**
     * @var string
     * @ORM\Column(name="password", type="string", length=255)
     */
    protected $password;

    /**
     * @var integer
     * @ORM\Column(name="query_port", type="smallint")
     */
    protected $queryPort;

    /**
     * @var integer
     * @ORM\Column(name="client_port", type="smallint")
     */
    protected $clientPort;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="TeamSpeakGroup", mappedBy="server", cascade={"persist"})
     */
    protected $groups;

    /**
     * @var DateTime
     * @ORM\Column(name="updated_at", type="datetime", options={"default":"2000-01-01 00:00:00"})
     */
    protected $updatedAt;

    public function __construct()
    {
        $this->groups = new ArrayCollection();
        $this->updatedAt = new DateTime('now');
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set service
     *
     * @param Service $service
     *
     * @return TeamSpeakServer
     */
    public function setService(Service $service = null)
    {
        $this->service = $service;

        return $this;
    }

    /**
     * Get service
     *
     * @return Service
     */
    public function getService()
    {
        return $this->service;
    }

    /**
     * @return string
     */
    public function getAlias()
    {
        return $this->alias;
    }

    /**
     * @param string $alias
     * @return $this
     */
    public function setAlias($alias)
    {
        $this->alias = $alias;
        return $this;
    }

    /**
     * @return string
     */
    public function getHost()
    {
        return $this->host;
    }

    /**
     * @param string $host
     * @return $this
     */
    public function setHost($host)
    {
        $this->host = $host;
        return $this;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param string $username
     * @return $this
     */
    public function setUsername($username)
    {
        $this->username = $username;
        return $this;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $password
     * @return $this
     */
    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @return int
     */
    public function getQueryPort()
    {
        return $this->queryPort;
    }

    /**
     * @param int $queryPort
     * @return $this
     */
    public function setQueryPort($queryPort)
    {
        $this->queryPort = $queryPort;
        return $this;
    }

    /**
     * @return int
     */
    public function getClientPort()
    {
        return $this->clientPort;
    }

    /**
     * @param int $clientPort
     * @return $this
     */
    public function setClientPort($clientPort)
    {
        $this->clientPort = $clientPort;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getGroups()
    {
        return $this->groups;
    }

    /**
     * @return ArrayCollection
     */
    public function getManagedGroups()
    {
        return $this->groups->filter(function(TeamSpeakGroup $group){
            return $group->isManagedGroup();
        });
    }

    /**
     * @param TeamSpeakGroup $group
     * @return $this
     */
    public function addGroup(TeamSpeakGroup $group)
    {
        $group->setServer($this);
        $this->groups->add($group);
        return $this;
    }

    /**
     * @param TeamSpeakGroup $group
     * @return $this
     */
    public function removeGroup(TeamSpeakGroup $group)
    {
        $this->groups->removeElement($group);
        $group->setServer(null);
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param DateTime $updatedAt
     */
    public function setUpdatedAt(DateTime $updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return string
     */
    public function generateConnectionUri()
    {
        return sprintf(
            'serverquery://%s:%s@%s:%d/?server_port=%d',
            $this->getUsername(),
            $this->getPassword(),
            $this->getHost(),
            $this->getQueryPort(),
            $this->getClientPort()
        );
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->alias;
    }
}
