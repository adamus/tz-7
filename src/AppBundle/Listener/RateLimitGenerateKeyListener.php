<?php
/**
 * Adding sessionId for RateLimit Key
 */

namespace AppBundle\Listener;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Noxlogic\RateLimitBundle\Events\GenerateKeyEvent;

class RateLimitGenerateKeyListener
{
    /**
     * @var RequestStack
     */
    protected $requestStack;

    /**
     * @var AuthorizationChecker
     */
    protected $checker;

    /**
     * @param RequestStack $requestStack
     * @param AuthorizationChecker $authorizationChecker
     */
    public function __construct(RequestStack $requestStack, AuthorizationChecker $authorizationChecker)
    {
        $this->requestStack = $requestStack;
        $this->checker = $authorizationChecker;
    }

    /**
     * @param GenerateKeyEvent $event
     */
    public function onGenerateKey(GenerateKeyEvent $event)
    {
        $key = $this->generateKey();

        $event->addToKey($key);
    }

    /**
     * @return string
     */
    protected function generateKey()
    {
        $request = $this->requestStack->getMasterRequest();

        if ($request instanceof Request) {
            $session = $request->getSession();
            $key = $session->getId();

            if ($this->checker->isGranted('ROLE_ADMIN') || $this->checker->isGranted('ROLE_SUPER_ADMIN')) {
                $key .= mt_rand(0, 1000);
            }

            return $key;
        }

        return "";
    }
}