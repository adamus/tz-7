<?php


namespace AppBundle\Repository;


use Doctrine\ORM\EntityRepository;
use Tz7\EveApiBundle\Model\CharacterInterface;
use AppBundle\Entity\TeamSpeakClient;


class TeamSpeakClientRepository extends EntityRepository
{
    /**
     * @param CharacterInterface $character
     * @param $uniqueId
     * @return null|TeamSpeakClient
     */
    public function findUserClient(CharacterInterface $character, $uniqueId)
    {
        return $this->findOneBy([
            'user' => $character,
            'uniqueId' => $uniqueId
        ]);
    }
}