<?php


namespace AppBundle\TeamSpeak;


use Doctrine\Common\Cache\CacheProvider;
use TeamSpeak3\Node\Server;
use TeamSpeak3\Node\Servergroup;
use TeamSpeak3\Node\Channel;
use TeamSpeak3\Node\Client;
use AppBundle\Entity\TeamSpeakServer;


/**
 * @link https://docs.planetteamspeak.com/ts3/php/framework/
 */
class TeamSpeakBridge
{
    const SERVER_DATA_CACHE_LIFETIME = 3600;
    const CLIENT_DATA_CACHE_LIFETIME = 60;

    /**
     * @var TeamSpeakServer
     */
    protected $serverEntity;

    /**
     * @var Server
     */
    protected $connection;

    /**
     * @var CacheProvider
     */
    protected $cache;

    /**
     * @var array
     */
    protected $runtimeCache = [];

    /**
     * @return TeamSpeakServer
     */
    public function getServerEntity()
    {
        return $this->serverEntity;
    }

    /**
     * @param TeamSpeakServer $serverEntity
     */
    public function setServerEntity($serverEntity)
    {
        $this->serverEntity = $serverEntity;
    }

    /**
     * @return Server
     */
    public function getConnection()
    {
        return $this->connection;
    }

    /**
     * @param Server $connection
     */
    public function setConnection($connection)
    {
        $this->connection = $connection;
    }

    /**
     * @return CacheProvider
     */
    public function getCache()
    {
        return $this->cache;
    }

    /**
     * @param CacheProvider $cache
     */
    public function setCache($cache)
    {
        $this->cache = $cache;
    }

    /**
     * @return Servergroup[]
     */
    public function getServerGroups()
    {
        $cacheKey = $this->generateCacheKey(__FUNCTION__);
        if (isset($this->runtimeCache[$cacheKey])) {
            return $this->runtimeCache[$cacheKey];
        }

        if (!($sgroupList = $this->readFromCache($cacheKey))) {
            $sgroupList = $this->connection->request("servergrouplist")->toAssocArray("sgid");
            $this->storeToCache($cacheKey, $sgroupList, self::SERVER_DATA_CACHE_LIFETIME);
        }

        $serverGroupList = [];
        foreach ($sgroupList as $sgid => $group) {
            $serverGroupList[$sgid] = new Servergroup($this->connection, $group);
        }
        $this->runtimeCache[$cacheKey] = $serverGroupList;

        return $serverGroupList;
    }

    /**
     * b_virtualserver_connectioninfo_view, b_virtualserver_token_use
     * b_channel_create_private, b_channel_create_modify_with_codec_opusvoice, b_channel_create_modify_with_codec_opusmusic
     * i_channel_modify_power
     * b_channel_join_permanent, b_channel_join_semi_permanent, b_channel_join_temporary
     * i_channel_join_power, i_channel_subscribe_power, i_channel_description_view_power
     * i_icon_id
     * b_group_is_permanent
     * i_group_auto_update_type
     * i_group_needed_modify_power, i_group_needed_member_add_power, i_group_needed_member_remove_power
     * i_client_max_clones_uid, i_client_max_avatar_filesize, i_client_max_channel_subscriptions
     * b_client_permissionoverview_view, b_client_permissionoverview_own
     * i_client_needed_serverquery_view_power, i_client_needed_kick_from_server_power, i_client_needed_kick_from_channel_power
     * i_client_needed_ban_power, i_client_needed_move_power, i_client_needed_complain_power
     * i_client_private_textmessage_power
     * b_client_channel_textmessage_send, b_client_offline_textmessage_send
     * i_client_poke_power
     * i_ft_file_upload_power, i_ft_file_download_power, i_ft_file_delete_power, i_ft_file_rename_power,
     * i_ft_file_browse_power, i_ft_quota_mb_download_per_client, i_ft_quota_mb_upload_per_client
     *
     * @param $sgid
     * @return array|mixed|null
     * @throws \Exception
     */
    public function getServerGroupPermListById($sgid)
    {
        $serverGroupList = $this->getServerGroups();
        if (isset($serverGroupList[$sgid])) {
            $cacheKey = $this->generateCacheKey(__FUNCTION__ . '|' . $sgid);
            if (!($permList = $this->readFromCache($cacheKey))) {
                $serverGroup = $serverGroupList[$sgid];
                $permList = $serverGroup->permList(true);
                $this->storeToCache($cacheKey, $permList, self::SERVER_DATA_CACHE_LIFETIME);
            }
            return $permList;

        } else {
            throw new \Exception(sprintf('%s: ServerGroup with id "%d" is not found', __METHOD__, $sgid));
        }
    }

    /**
     * @return Channel[]
     */
    public function getChannelList()
    {
        $cacheKey = $this->generateCacheKey(__FUNCTION__);
        if (isset($this->runtimeCache[$cacheKey])) {
            return $this->runtimeCache[$cacheKey];
        }

        if (!($channels = $this->readFromCache($cacheKey))) {
            $channels = $this->connection->request("channellist -topic -flags -voice -limits -icon")->toAssocArray("cid");
            $this->storeToCache($cacheKey, $channels, self::SERVER_DATA_CACHE_LIFETIME);
        }

        $serverChannels = [];
        foreach ($channels as $cid => $channel) {
            $serverChannels[$cid] = new Channel($this->connection, $channel);
        }

        $this->runtimeCache[$cacheKey] = $serverChannels;
        return $serverChannels;
    }

    /**
     * i_channel_needed_permission_modify_power
     * i_channel_needed_delete_power
     * i_channel_needed_join_power
     * i_channel_needed_subscribe_power
     * i_channel_needed_description_view_power
     * i_icon_id
     * i_client_needed_talk_power
     *       'permsid' =>
     *           object(TeamSpeak3\Helper\String)[1691]
     *           protected 'string' => string 'i_channel_needed_join_power' (length=27)
     *           protected 'position' => int 0
     *       'permvalue' => int 59
     *       'permnegated' => int 0
     *       'permskip' => int 0
     * @return array
     */
    public function getChannelsPermList()
    {
        $cacheKey = $this->generateCacheKey(__FUNCTION__);
        if (isset($this->runtimeCache[$cacheKey])) {
            return $this->runtimeCache[$cacheKey];
        }

        if (!($channelsPermList = $this->readFromCache($cacheKey))) {
            $channelsPermList = [];
            foreach ($this->getChannelList() as $cid => $channel) {
                $channelsPermList[$cid] = $channel->permList(true);
                usleep(500000);
            }
            $this->storeToCache($cacheKey, $channelsPermList, self::SERVER_DATA_CACHE_LIFETIME);
        }

        $this->runtimeCache[$cacheKey] = $channelsPermList;
        return $channelsPermList;
    }

    /**
     * @param int $cid
     * @return null|Channel
     */
    public function getChannelById($cid)
    {
        $channelList = $this->getChannelList();
        return isset($channelList[$cid]) ? $channelList[$cid] : null;
    }

    /**
     * @param int $cid
     * @return null|array
     */
    public function getChannelPermListById($cid)
    {
        $channelsPermList = $this->getChannelsPermList();
        return isset($channelsPermList[$cid]) ? $channelsPermList[$cid] : null;
    }

    /**
     * @return Client[]
     */
    public function getClientList()
    {
        $cacheKey = $this->generateCacheKey(__FUNCTION__);
        if (isset($this->runtimeCache[$cacheKey])) {
            return $this->runtimeCache[$cacheKey];
        }

        if (!($clients = $this->readFromCache($cacheKey)))
        {
            $clients = $this->connection->request(
                "clientlist -uid -away -badges -voice -info -times -groups -icon -country -ip"
            )->toAssocArray("clid");

            $this->storeToCache($cacheKey, $clients, self::CLIENT_DATA_CACHE_LIFETIME);
        }

        $clientList = [];
        foreach ($clients as $client) {
            $clientList[] = new Client($this->connection, $client);
        }

        $this->runtimeCache[$cacheKey] = $clientList;

        return $clientList;
    }

    /**
     * @param Client $client
     * @return int[]
     */
    public function clientGetServerGroups(Client $client)
    {
        return explode(",", $client["client_servergroups"]);
    }

    /**
     * @param $postfix
     */
    public function resetCache($postfix)
    {
        $this->storeToCache($this->generateCacheKey($postfix), null, 1);
    }

    /**
     * @param string $cacheKey
     * @return mixed|null
     */
    protected function readFromCache($cacheKey)
    {
        if (!($raw = $this->cache->fetch($cacheKey))) {
            return null;
        } else {
            return unserialize($raw);
        }
    }

    /**
     * @param string $cacheKey
     * @param mixed $data
     * @param int $lifetime
     */
    protected function storeToCache($cacheKey, $data, $lifetime = -1)
    {
        $this->cache->save($cacheKey, serialize($data), $lifetime);
    }

    /**
     * @param string $postfix
     * @return string
     */
    protected function generateCacheKey($postfix='')
    {
        return sprintf('%s|%s|BRIDGE|%s',
            __CLASS__,
            sha1($this->serverEntity?$this->serverEntity->generateConnectionUri():''),
            $postfix
        );
    }
}