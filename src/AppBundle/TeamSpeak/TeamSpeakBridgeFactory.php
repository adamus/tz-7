<?php


namespace AppBundle\TeamSpeak;


use Doctrine\ORM\EntityManager;
use Doctrine\Common\Cache\CacheProvider;
use Doctrine\ORM\EntityNotFoundException;
use Symfony\Bridge\Monolog\Logger;
use JMS\DiExtraBundle\Annotation as DI;
use TeamSpeak3\TeamSpeak3;

use AppBundle\Entity\TeamSpeakServer;


/**
 * @DI\Service("ts_bridge_factory")
 */
class TeamSpeakBridgeFactory
{
    const CACHE_PREFIX = __CLASS__;
    const ADAPTER_CACHE_LIFETIME = 3600;

    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var array
     */
    protected $runtimeCache = [];

    /**
     * @DI\InjectParams({
     *     "em"     = @DI\Inject("doctrine.orm.entity_manager"),
     *     "logger" = @DI\Inject("logger"),
     *     "cache"  = @DI\Inject("doctrine_cache.providers.teamspeak_cache"),
     * })
     *
     * @param EntityManager $em
     * @param Logger $logger
     * @param CacheProvider $cache
     */
    public function __construct(EntityManager $em, Logger $logger, CacheProvider $cache)
    {
        $this->em = $em;
        $this->logger = $logger;
        $this->cache = $cache;
    }

    /**
     * @param $id
     * @return TeamSpeakBridge
     * @throws EntityNotFoundException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\TransactionRequiredException
     */
    public function buildBridgeForServerId($id)
    {
        if (isset($this->runtimeCache[$id])) {
            return $this->runtimeCache[$id];

        } else {
            $this->logger->debug(sprintf('%s: "%d"', __METHOD__, $id));
            /** @var TeamSpeakServer $serverEntity */
            $serverEntity = $this->em->find('AppBundle:TeamSpeakServer', $id);
            if ($serverEntity) {
                return $this->buildBridgeForServer($serverEntity);

            } else {
                throw new EntityNotFoundException(sprintf('%s: TeamSpeakServer not found for id "%d"', __METHOD__, $id));
            }
        }
    }

    /**
     * @param TeamSpeakServer $server
     * @return TeamSpeakBridge
     */
    public function buildBridgeForServer(TeamSpeakServer $server)
    {
        if (isset($this->runtimeCache[$server->getId()])) {
            return $this->runtimeCache[$server->getId()];

        } else {
            $cacheKey = $this->generateServerCacheKey($server);
            $this->logger->debug(sprintf('%s: Cache Key is "%s"', __METHOD__, $cacheKey));

            if (!($adapter = unserialize($this->cache->fetch($cacheKey)))) {
                $this->logger->info(sprintf('%s: Build TS3 Adapter with uri "%s"', __METHOD__, $server->generateConnectionUri()));
                $adapter = TeamSpeak3::factory($server->generateConnectionUri());
                $this->cache->save($cacheKey, serialize($adapter), self::ADAPTER_CACHE_LIFETIME);
            }

            $bridge = new TeamSpeakBridge();
            $bridge->setServerEntity($server);
            $bridge->setCache($this->cache);
            $bridge->setConnection($adapter);

            $this->runtimeCache[$server->getId()] = $bridge;

            return $bridge;
        }
    }

    /**
     * @param TeamSpeakServer $server
     * @return string
     */
    protected function generateServerCacheKey(TeamSpeakServer $server)
    {
        return sprintf('%s|%s|ADAPTER', self::CACHE_PREFIX, sha1($server->generateConnectionUri()));
    }
}