<?php


namespace AppBundle\TeamSpeak;


use Doctrine\ORM\EntityManager;
use Symfony\Bridge\Monolog\Logger;
use JMS\DiExtraBundle\Annotation as DI;

use TeamSpeak3\Node\Client;
use Tz7\EveCriterionBundle\Service\CriteriaCheckerInterface;

use AppBundle\Entity\TeamSpeakClient;
use AppBundle\Entity\TeamSpeakGroup;
use AppBundle\Entity\TeamSpeakServer;
use UserBundle\Entity\User;

/**
 * @DI\Service("ts_client_data")
 */
class TeamSpeakClientData
{
    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var TeamSpeakBridgeFactory
     */
    protected $bridgeFactory;

    /**
     * @var CriteriaCheckerInterface
     */
    protected $criteriaChecker;

    /**
     * @var Logger
     */
    protected $logger;

    /**
     * @DI\InjectParams({
     *     "em" = @DI\Inject("doctrine.orm.entity_manager"),
     *     "bridgeFactory" = @DI\Inject("ts_bridge_factory"),
     *     "criteriaChecker" = @DI\Inject("tz7.eve_criterion.criteria_checker"),
     *     "logger" = @DI\Inject("logger")
     * })
     * @param EntityManager $em
     * @param TeamSpeakBridgeFactory $bridgeFactory
     * @param CriteriaCheckerInterface $criteriaChecker
     * @param Logger $logger
     */
    public function __construct(
        EntityManager $em,
        TeamSpeakBridgeFactory $bridgeFactory,
        CriteriaCheckerInterface $criteriaChecker,
        Logger $logger)
    {
        $this->em = $em;
        $this->bridgeFactory = $bridgeFactory;
        $this->criteriaChecker = $criteriaChecker;
        $this->logger = $logger;
    }

    /**
     * @param TeamSpeakServer $server
     * @param bool $resetCache
     * @return Client[]
     */
    public function getClientsOfServer(TeamSpeakServer $server, $resetCache = false)
    {
        $bridge = $this->bridgeFactory->buildBridgeForServer($server);

        if ($resetCache) {
            $bridge->resetCache('getClientList');
        }

        return $bridge->getClientList();
    }

    /**
     * @param User $user
     * @param Client[] $clientList
     * @return Client[]
     */
    public function matchClientForUser(User $user, array $clientList)
    {
        $matches = [];
        foreach ($clientList as $client) {
            $nickName = (string)$client->getProperty('client_nickname');
            if (stripos($nickName, $user->getUsername()) !== false) {
                $matches[] = $client;
            }
        }
        return $matches;
    }

    /**
     * @param TeamSpeakClient $entity
     * @param TeamSpeakServer $server
     */
    public function checkUserGroups(TeamSpeakClient $entity, TeamSpeakServer $server)
    {
        $clientList = $this->getClientsOfServer($server);
        foreach ($clientList as $client) {
            if (((string)$client->getProperty('client_unique_identifier')) == $entity->getUniqueId()) {
                $this->checkUserClientGroups($entity, $client, $server);
                break;
            }
        }
    }

    /**
     * @param Client $client
     * @param TeamSpeakServer $server
     * @return boolean
     */
    public function checkClientGroups(Client $client, TeamSpeakServer $server)
    {
        $this->logger->debug(sprintf('%s: Checking "%s" on %s', __METHOD__, $client, $server->getAlias()));

        /** @var TeamSpeakClient $entity */
        $entity = $this->em
            ->getRepository('AppBundle:TeamSpeakClient')
            ->findOneBy(['uniqueId'=>(string)$client->getProperty('client_unique_identifier')])
        ;

        if ($entity) {
            $this->checkUserClientGroups($entity, $client, $server);
            return true;
        } else {
            $client->message('[COLOR=red]ERROR:[/COLOR] Client not connected to any character!');
            $url = 'http://' . $server->getHost();
            $client->message(sprintf('Please register at [url=%s]%s[/url]', $url, $url));
            return false;
        }
    }

    /**
     * @param TeamSpeakClient $entity
     * @param Client $client
     * @param TeamSpeakServer $server
     */
    public function checkUserClientGroups(TeamSpeakClient $entity, Client $client, TeamSpeakServer $server)
    {
        $this->logger->debug(sprintf('%s: "%s" on "%s"', __METHOD__, $entity->getUser()->getUsername(), $server->getHost()));
        $bridge = $this->bridgeFactory->buildBridgeForServer($server);
        $nickName = (string)$client['client_nickname'];

        $cid = intval($client->getProperty('client_channel_group_inherited_channel_id'));
        $currentChannel = $bridge->getChannelById($cid);
        $currentChannelPermList = $bridge->getChannelPermListById($cid);
        $currentChannelNeededJoinPower = intval(@$currentChannelPermList['i_channel_needed_join_power']['permvalue']);

        $maxJoinPower = 0;
        $clientServerGroups = $bridge->clientGetServerGroups($client);
        foreach ($clientServerGroups as $i => $serverGroupId)
        {
            /** @var TeamSpeakGroup $serverGroupEntity */
            $serverGroupEntity = $server->getGroups()->filter(function(TeamSpeakGroup $group) use ($serverGroupId) {
                return $group->getServerGroupId() == $serverGroupId;
            })->first();

            if ($serverGroupEntity) {
                try {
                    if ($serverGroupEntity->isManagedGroup()) {
                        if ($this->criteriaChecker->testCriteria(
                                $entity->getUser(),
                                $serverGroupEntity->getCriteria(),
                                (string)$client['client_nickname'])
                        ) {
                            $this->logger->debug(sprintf(
                                '%s: Checked "%s" membership of "%s"', __METHOD__,
                                $nickName, $serverGroupEntity->getServerGroupName()
                            ));

                        } else {
                            $this->logger->warning(sprintf(
                                '%s: "%s" should not be member of "%s" group!', __METHOD__,
                                $nickName, $serverGroupEntity->getServerGroupName()
                            ));
                            $client->message(sprintf('[COLOR=red]ERROR:[/COLOR] Should not be member of "%s" group.', $serverGroupEntity->getServerGroupName()));
                            $client->remServerGroup($serverGroupId);
                            unset($clientServerGroups[$i]);
                            continue; // Skip join power
                        }
                    } else {
                        $this->logger->info(sprintf('%s: "%s" group for "%s" is not managed',
                            __METHOD__, $serverGroupEntity->getServerGroupName(), $nickName
                        ));
                    }

                    $groupPermList = $bridge->getServerGroupPermListById($serverGroupId);
                    $joinPower = intval(@$groupPermList['i_channel_join_power']['permvalue']);
                    if ($joinPower > $maxJoinPower) {
                        $maxJoinPower = $joinPower;
                    }

                } catch (\Exception $ex) {
                    $this->logger->error(sprintf('%s: "%s"', __METHOD__, $ex->getMessage()));
                }
            } else {
                $this->logger->error(sprintf('%s: Missing serverGroup "%s"', __METHOD__, $serverGroupId));
            }
        }

        /** @var TeamSpeakGroup $serverGroupEntity */
        foreach ($server->getManagedGroups() as $serverGroupEntity)
        {
            $sgid = $serverGroupEntity->getServerGroupId();
            if ($this->criteriaChecker->testCriteria(
                    $entity->getUser(),
                    $serverGroupEntity->getCriteria(),
                    (string)$client['client_nickname'])
                && !in_array($sgid, $clientServerGroups)
            ) {
                $this->logger->debug(sprintf(
                    '%s: Added "%s" membership for "%s"', __METHOD__,
                    $serverGroupEntity->getServerGroupName(), $nickName
                ));

                try {
                    $client->addServerGroup($sgid);
                    $client->message(sprintf(
                        '[COLOR=green]INFO:[/COLOR] "%s" membership granted',
                        $serverGroupEntity->getServerGroupName()
                    ));

                    $groupPermList = $bridge->getServerGroupPermListById($sgid);
                    $joinPower = intval(@$groupPermList['i_channel_join_power']['permvalue']);
                    if ($joinPower > $maxJoinPower) {
                        $maxJoinPower = $joinPower;
                    }
                } catch (\Exception $ex) {
                    $this->logger->error(sprintf('%s: "%s"', __METHOD__, $ex->getMessage()));
                }
            }
        }

        if ($currentChannelNeededJoinPower > $maxJoinPower)
        {
            $client->message(sprintf(
                '[COLOR=red]ERROR:[/COLOR] Insufficient permissions to be in "%s" channel',
                (string)$currentChannel
            ));
            $client->kick();
        }
    }

    /**
     * @param Client $client
     * @param TeamSpeakServer $server
     */
    public function revokeClientGroups(Client $client, TeamSpeakServer $server)
    {
        $client->message('Revoking groups.');

        $bridge = $this->bridgeFactory->buildBridgeForServer($server);
        $clientServerGroups = $bridge->clientGetServerGroups($client);
        /** @var TeamSpeakGroup $serverGroupEntity */
        foreach ($server->getManagedGroups() as $serverGroupEntity)
        {
            $sgid = $serverGroupEntity->getServerGroupId();
            if (in_array($sgid,$clientServerGroups)) {
                try {
                    $this->logger->debug(sprintf(
                        '%s: Remove group #%d from "%s"', __METHOD__, $sgid,
                        (string)$client->getProperty('client_nickname')
                    ));
                    $client->remServerGroup($sgid);
                } catch (\Exception $ex) {
                    $this->logger->error(sprintf('%s: "%s"', __METHOD__, $ex->getMessage()));
                }
            }
        }
    }

    /**
     * @param TeamSpeakServer $server
     */
    public function checkClientsOfServer(TeamSpeakServer $server)
    {
        $clientList = $this->getClientsOfServer($server, true);
        $this->logger->debug(sprintf('%s %s', __METHOD__, $server->getAlias()));
        foreach ($clientList as $client)
        {
            try {
                if ($client->getProperty('client_version') != 'ServerQuery') {
                    if (!$this->checkClientGroups($client, $server)) {
                        $this->revokeClientGroups($client, $server);
                    }
                }
            } catch (\Exception $ex) {
                $this->logger->error(sprintf('%s: "%s" failed checking "%s"', __METHOD__, $client, $ex->getMessage()));
            }
        }
    }
}