<?php


namespace AppBundle\TeamSpeak;


use Symfony\Bridge\Monolog\Logger;
use JMS\DiExtraBundle\Annotation as DI;

use AppBundle\Entity\TeamSpeakGroup;
use AppBundle\Entity\TeamSpeakServer;

/**
 * @DI\Service("ts_server_data")
 */
class TeamSpeakServerData
{
    /**
     * @var TeamSpeakBridgeFactory
     */
    protected $bridgeFactory;

    /**
     * @var Logger
     */
    protected $logger;

    /**
     * @DI\InjectParams({
     *     "bridgeFactory" = @DI\Inject("ts_bridge_factory"),
     *     "logger" = @DI\Inject("logger")
     * })
     *
     * @param TeamSpeakBridgeFactory $bridgeFactory
     * @param Logger $logger
     */
    public function __construct(TeamSpeakBridgeFactory $bridgeFactory, Logger $logger)
    {
        $this->bridgeFactory = $bridgeFactory;
        $this->logger = $logger;
    }

    /**
     * @param TeamSpeakServer $server
     */
    public function updateServerGroups(TeamSpeakServer $server)
    {
        $bridge = $this->bridgeFactory->buildBridgeForServer($server);
        $serverGroups = $bridge->getServerGroups();

        /** @var TeamSpeakGroup $tsGroup */
        foreach ($server->getGroups() as $tsGroup)
        {
            $sgid = $tsGroup->getServerGroupId();
            if (!isset($serverGroups[$sgid])) {
                $server->removeGroup($tsGroup);

            } else {
                $serverGroup = $serverGroups[$sgid];
                $tsGroup->setServerGroupName($serverGroup->__toString());
                unset($serverGroups[$sgid]);
            }
        }

        foreach ($serverGroups as $sgid => $serverGroup)
        {
            $tsGroup = new TeamSpeakGroup();
            $tsGroup->setServerGroupId($sgid);
            $tsGroup->setServerGroupName($serverGroup->__toString());
            $tsGroup->setServer($server);
            $server->addGroup($tsGroup);
        }
    }
}