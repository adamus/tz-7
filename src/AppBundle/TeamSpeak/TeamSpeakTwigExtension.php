<?php


namespace AppBundle\TeamSpeak;


use Symfony\Component\DependencyInjection\Container;
use JMS\DiExtraBundle\Annotation as DI;

use Tz7\EveCriterionBundle\Service\CriteriaCheckerInterface;

use TeamSpeak3\Node\Client;
use UserBundle\Entity\User;
use AppBundle\Entity\Criterion;
use AppBundle\Entity\TeamSpeakGroup;

/**
 * @DI\Service
 * @DI\Tag("twig.extension")
 */
class TeamSpeakTwigExtension extends \Twig_Extension
{
    /**
     * @var Container
     */
    protected $container;

    /**
     * @DI\InjectParams({"container" = @DI\Inject("service_container")})
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    /**
     * @return array
     */
    public function getFunctions()
    {
        $config = array(
            'is_safe' => array('html'),
        );

        return array(
            new \Twig_SimpleFunction('tsGroupMatch', array($this, 'tsGroupMatch'), $config),
            new \Twig_SimpleFunction('tsCriterionMatch', array($this, 'tsCriterionMatch'), $config),
        );
    }

    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('tsResolveCondition', array($this, 'tsResolveCondition')),
            new \Twig_SimpleFilter('resolveMatcher', array($this, 'resolveMatcher')),
        );
    }

    /**
     * @param User $user
     * @param Client $client
     * @param TeamSpeakGroup $group
     * @return bool
     */
    public function tsGroupMatch(User $user, Client $client, TeamSpeakGroup $group)
    {
        return $this->getCriteriaChecker()->testCriteria($user, $group->getCriteria(), (string)$client['client_nickname']);
    }

    /**
     * @param User $user
     * @param Client $client
     * @param Criterion $criterion
     * @return bool
     */
    public function tsCriterionMatch(User $user, Client $client, Criterion $criterion)
    {
        return $this->getCriteriaChecker()->testCriterion($user, $criterion, (string)$client['client_nickname']);
    }

    /**
     * @param Criterion $criterion
     * @param User $user
     * @return bool|mixed|string
     */
    public function tsResolveCondition(Criterion $criterion, User $user)
    {
        return $this->getCriteriaChecker()->resolveCondition($user, $criterion);
    }

    /**
     * @param Criterion $criterion
     * @param User $user
     * @param Client $client
     * @return bool|mixed|string
     */
    public function resolveMatcher(Criterion $criterion, User $user, Client $client)
    {
        return $this->getCriteriaChecker()->resolveMatcher($user, $criterion, (string)$client['client_nickname']);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ts_twig_extension';
    }

    /**
     * @return CriteriaCheckerInterface
     */
    protected function getCriteriaChecker()
    {
        return $this->container->get('tz7.eve_criterion.criteria_checker');
    }

    /**
     * @return TeamSpeakClientData
     */
    protected function tsClientData()
    {
        return $this->container->get('ts_client_data');
    }
}