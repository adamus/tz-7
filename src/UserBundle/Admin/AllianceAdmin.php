<?php
namespace UserBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;

use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Form\FormMapper;

class AllianceAdmin extends Admin
{
    public function configureListFields( ListMapper $listMapper )
    {
        $listMapper
            ->addIdentifier('id')
            ->add('name')
            ->add('short')
        ;
    }

    public function configureDatagridFilters( DatagridMapper $datagridMapper )
    {
        $datagridMapper
            ->add('id')
            ->add('name')
            ->add('short')
        ;
    }

    public function configureFormFields( FormMapper $formMapper )
    {
        $formMapper
            ->add('id')
            ->add('name')
            ->add('short')
        ;
    }

    public function configureShowFields( ShowMapper $showMapper )
    {
        $showMapper
            ->add('id')
            ->add('name')
            ->add('short')
        ;
    }
}
