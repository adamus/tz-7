<?php
namespace UserBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;

use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Form\FormMapper;

class CorporationAdmin extends Admin
{
    public function configureListFields( ListMapper $listMapper )
    {
        $listMapper
            ->addIdentifier('id')
            ->add('name')
            ->add('ticker')
            ->add('alliance')
        ;
    }

    public function configureDatagridFilters( DatagridMapper $datagridMapper )
    {
        $datagridMapper
            ->add('id')
            ->add('name')
            ->add('ticker')
            ->add('alliance')
        ;
    }

    public function configureFormFields( FormMapper $formMapper )
    {
        $formMapper
            ->add('id')
            ->add('name')
            ->add('ticker')
            ->add('alliance', 'sonata_type_model_list')
            ->add('ceo', 'sonata_type_model_list')
            ->add('updatedAt','datetime',[
                'date_widget' => 'single_text',
                'time_widget' => 'single_text',
                'date_format' => 'yyyy-MM-dd',
                'attr' => ['class' => 'datepicker'],
                'label' => 'Dátum'
            ])
        ;
    }

    public function configureShowFields( ShowMapper $showMapper )
    {
        $showMapper
            ->add('id')
            ->add('name')
            ->add('ticker')
            ->add('alliance')
        ;
    }
}
