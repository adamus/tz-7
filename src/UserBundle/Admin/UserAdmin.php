<?php


namespace UserBundle\Admin;


use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class UserAdmin extends Admin
{
    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $hierarchy = $this->getConfigurationPool()->getContainer()->getParameter('security.role_hierarchy.roles');
        $roles = array();
        foreach (array_keys($hierarchy) as $role) {
            $roles[$role] = $role;
        }

        $formMapper
            ->add('username')
            ->add('enabled')
            ->add('corporation', 'sonata_type_model_list')
            ->add('director', 'checkbox', ['required'=>false])
            ->add('updatedAt','datetime',[
                'date_widget' => 'single_text',
                'time_widget' => 'single_text',
                'date_format' => 'yyyy-MM-dd',
                'attr' => ['class' => 'datepicker'],
                'label' => 'Dátum'
            ])
        ;

        if ($this->isGranted('ROLE_SUPERADMIN')) {
            $formMapper->add('roles', 'choice', array(
                'choices' => $roles,
                'multiple' => true,
                'required' => false
            ));
        }
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('username')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->add('username')
        ;
    }

}