<?php

namespace UserBundle\Entity;


use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Tz7\EveApiBundle\Model\AllianceInterface;
use Tz7\EveApiBundle\Model\CorporationInterface;
use Tz7\EveApiBundle\Model\Mail\AllianceRecipientInterface;
use Tz7\EveApiBundle\Model\Mail\MailInterface;


/**
 * @ORM\Table(name="user_alliance")
 * @ORM\Entity
 */
class Alliance implements AllianceInterface, AllianceRecipientInterface
{
    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(name="name", type="string")
     */
    protected $name;

    /**
     * @var string
     * @ORM\Column(name="short", type="string")
     */
    protected $short;

    /**
     * @var array[Corporation]
     * @ORM\OneToMany(targetEntity="Corporation", mappedBy="alliance")
     */
    protected $members;

    /**
     * @var DateTimeInterface
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    protected $updatedAt;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="Mail", mappedBy="toAlliance")
     */
    protected $mails;

    /**
     * Constructor
     *
     * @param int $allianceID
     */
    public function __construct($allianceID = 0)
    {
        $this->id      = $allianceID;
        $this->short   = "???";
        $this->members = new ArrayCollection();
        $this->mails   = new ArrayCollection();
    }

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param integer $id
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return Alliance
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getShort()
    {
        return $this->short;
    }

    /**
     * @param string $short
     *
     * @return Alliance
     */
    public function setShort($short)
    {
        $this->short = $short;

        return $this;
    }

    /**
     * @return Corporation[]
     */
    public function getMembers()
    {
        return $this->members;
    }

    /**
     * @param array $members
     *
     * @return Alliance
     */
    public function setMembers($members)
    {
        $this->members = $members;

        return $this;
    }

    /**
     * @param CorporationInterface $corporation
     *
     * @return $this
     */
    public function addMember(CorporationInterface $corporation)
    {
        $corporation->setAlliance($this);
        $this->members[] = $corporation;

        return $this;
    }

    /**
     * @param CorporationInterface $corporation
     *
     * @return $this
     */
    public function removeMember(CorporationInterface $corporation)
    {
        $this->members->removeElement($corporation);

        return $this;
    }

    /**
     * @return DateTimeInterface
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param DateTimeInterface $updatedAt
     *
     * @return $this
     */
    public function setUpdatedAt(DateTimeInterface $updatedAt = null)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getMails()
    {
        return $this->mails;
    }

    /**
     * @param MailInterface $mail
     *
     * @return $this
     */
    public function addMail(MailInterface $mail)
    {
        $this->mails->add($mail);
        $mail->setToAlliance($this);

        return $this;
    }

    /**
     * @param MailInterface $mail
     *
     * @return $this
     */
    public function removeMail(MailInterface $mail)
    {
        $this->mails->removeElement($mail);
        $mail->setToAlliance(null);

        return $this;
    }

    /**
     * @return string
     */
    function __toString()
    {
        return (string) $this->getName();
    }
}
