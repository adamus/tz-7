<?php

namespace UserBundle\Entity;


use DateTime;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Tz7\EveApiBundle\Model\AllianceInterface;
use Tz7\EveApiBundle\Model\CharacterInterface;
use Tz7\EveApiBundle\Model\CorporationInterface;
use Tz7\EveApiBundle\Model\Mail\CorporationRecipientInterface;
use Tz7\EveApiBundle\Model\Mail\MailInterface;


/**
 * @ORM\Table(name="user_corporation")
 * @ORM\Entity
 */
class Corporation implements CorporationInterface, CorporationRecipientInterface
{
    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     */
    protected $id;

    /**
     * @var Alliance
     * @ORM\ManyToOne(targetEntity="Alliance", inversedBy="members", cascade={"persist"})
     * @ORM\JoinColumn(name="alliance_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $alliance;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="User", mappedBy="corporation")
     */
    protected $members;

    /**
     * @var User
     * @ORM\OneToOne(targetEntity="User")
     */
    protected $ceo;

    /**
     * @var string
     * @ORM\Column(name="name", type="string")
     */
    protected $name;

    /**
     * @var string
     * @ORM\Column(name="ticker", type="string")
     */
    protected $ticker;

    /**
     * @var DateTimeInterface
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    protected $updatedAt;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="Mail", mappedBy="toCorporation")
     */
    protected $mails;

    /**
     * Constructor
     *
     * @param int $corporationID
     */
    public function __construct($corporationID = 0)
    {
        $this->id        = $corporationID;
        $this->ticker    = "???";
        $this->members   = new ArrayCollection();
        $this->updatedAt = (new DateTime('now'))->modify('-1 year');
        $this->mails     = new ArrayCollection();
    }

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param integer $id
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return Alliance
     */
    public function getAlliance()
    {
        return $this->alliance;
    }

    /**
     * @param AllianceInterface $alliance
     *
     * @return $this
     */
    public function setAlliance(AllianceInterface $alliance = null)
    {
        $this->alliance = $alliance;

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getMembers()
    {
        return $this->members;
    }

    /**
     * @param array $members
     *
     * @return Corporation
     */
    public function setMembers($members)
    {
        $this->members = $members;

        return $this;
    }

    /**
     * @param CharacterInterface $user
     *
     * @return $this
     */
    public function addMember(CharacterInterface $user)
    {
        $user->setCorporation($this);
        $this->members[] = $user;

        return $this;
    }

    /**
     * @param CharacterInterface $user
     *
     * @return $this
     */
    public function removeMember(CharacterInterface $user)
    {
        $this->members->removeElement($user);

        return $this;
    }

    /**
     * @return User
     */
    public function getCeo()
    {
        return $this->ceo;
    }

    /**
     * @param User $ceo
     *
     * @return Corporation
     */
    public function setCeo($ceo)
    {
        $this->ceo = $ceo;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return Corporation
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getTicker()
    {
        return $this->ticker;
    }

    /**
     * @param string $ticker
     *
     * @return Corporation
     */
    public function setTicker($ticker)
    {
        $this->ticker = $ticker;

        return $this;
    }

    /**
     * @return DateTimeInterface
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param DateTimeInterface $updatedAt
     *
     * @return Corporation
     */
    public function setUpdatedAt(DateTimeInterface $updatedAt = null)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getMails()
    {
        return $this->mails;
    }

    /**
     * @param MailInterface $mail
     *
     * @return $this
     */
    public function addMail(MailInterface $mail)
    {
        $this->mails->add($mail);
        $mail->setToCorporation($this);

        return $this;
    }

    /**
     * @param MailInterface $mail
     *
     * @return $this
     */
    public function removeMail(MailInterface $mail)
    {
        $this->mails->removeElement($mail);
        $mail->setToCorporation(null);

        return $this;
    }

    /**
     * @return string
     */
    function __toString()
    {
        return (string) $this->getName();
    }
}
