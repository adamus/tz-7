<?php


namespace UserBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use Tz7\EveDiscordBundle\Model\AbstractCharacterAuthenticationToken;


/**
 * @ORM\Table(name="tz7_discord_character_auth")
 * @ORM\Entity(repositoryClass="Tz7\EveDiscordBundle\Repository\CharacterAuthenticationTokenRepository")
 */
class DiscordCharacterAuthToken extends AbstractCharacterAuthenticationToken
{

}
