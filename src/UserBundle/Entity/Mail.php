<?php


namespace UserBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Tz7\EveApiBundle\Model\Mail\AbstractMail;


/**
 * @ORM\Table(name="tz7_eve_api_mail")
 * @ORM\Entity(repositoryClass="Tz7\EveApiBundle\Repository\MailRepository")
 */
class Mail extends AbstractMail
{
    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="User", inversedBy="mails")
     * @ORM\JoinTable(
     *     name="tz7_eve_api_mail_recipient_character",
     *     joinColumns = {
     *         @ORM\JoinColumn(
     *             name="mail_id",
     *             referencedColumnName="id",
     *             onDelete="CASCADE"
     *         )
     *     },
     *     inverseJoinColumns = {
     *         @ORM\JoinColumn(
     *             name="character_id",
     *             referencedColumnName="id",
     *             onDelete="CASCADE"
     *         )
     *     }
     * )
     */
    protected $toCharacters;
}