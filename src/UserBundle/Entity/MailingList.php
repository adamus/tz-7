<?php


namespace UserBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Tz7\EveApiBundle\Model\Mail\AbstractMailingList;


/**
 * @ORM\Table(name="tz7_eve_api_mailing_list")
 * @ORM\Entity()
 */
class MailingList extends AbstractMailingList
{
    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="User", inversedBy="mailingLists")
     * @ORM\JoinTable(
     *     name="tz7_eve_api_mailing_list_member",
     *     joinColumns = {
     *         @ORM\JoinColumn(
     *             name="mailing_list_id",
     *             referencedColumnName="id",
     *             onDelete="CASCADE"
     *         )
     *     },
     *     inverseJoinColumns = {
     *         @ORM\JoinColumn(
     *             name="character_id",
     *             referencedColumnName="id",
     *             onDelete="CASCADE"
     *         )
     *     }
     * )
     */
    protected $memberCharacters;
}