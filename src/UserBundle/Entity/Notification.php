<?php


namespace UserBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use Tz7\EveApiBundle\Model\Notification\AbstractNotification;


/**
 * @ORM\Table(name="tz7_eve_api_notification")
 * @ORM\Entity(repositoryClass="Tz7\EveApiBundle\Repository\NotificationRepository")
 */
class Notification extends AbstractNotification
{

}