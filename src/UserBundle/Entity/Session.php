<?php

namespace UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="session")
 * @ORM\Entity
 */
class Session
{
    /**
     * @ORM\Column(name="session_id", type="string", length=255)
     * @ORM\Id
     * @var string $id
     */
    protected $id;

    /**
     * @ORM\Column(name="session_value", type="text")
     * @var string
     */
    protected $value;

    /**
     * @ORM\Column(name="session_time", type="integer")
     * @var string
     */
    protected $time;

    /**
     * @ORM\Column(name="sess_lifetime", type="integer")
     * @var string
     */
    protected $lifeTime;

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param string $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * @return string
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * @param string $time
     */
    public function setTime($time)
    {
        $this->time = $time;
    }

    /**
     * @return string
     */
    public function getLifeTime()
    {
        return $this->lifeTime;
    }

    /**
     * @param string $lifeTime
     */
    public function setLifeTime($lifeTime)
    {
        $this->lifeTime = $lifeTime;
    }
}