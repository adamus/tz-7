<?php

namespace UserBundle\Entity;


use AppBundle\Entity\TeamSpeakClient;
use DateTime;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints as DA;
use Symfony\Component\Validator\Constraints as Assert;
use Tz7\EveApiBundle\Model\Api\ApiKeyOwnerInterface;
use Tz7\EveApiBundle\Model\CharacterInterface;
use Tz7\EveApiBundle\Model\CharacterKeysTrait;
use Tz7\EveApiBundle\Model\CorporationInterface;
use Tz7\EveApiBundle\Model\Mail\CharacterRecipientInterface;
use Tz7\EveApiBundle\Model\Mail\MailingListInterface;
use Tz7\EveApiBundle\Model\Mail\MailInterface;
use Tz7\EveApiBundle\Model\Notification\NotificationOwnerInterface;
use Tz7\EveApiBundle\Model\Notification\NotificationOwnerTrait;


/**
 * @ORM\Entity
 * @ORM\Table(name="user")
 * @DA\UniqueEntity(fields="username", message="A megadott felhasználónév már használatban van.")
 * @DA\UniqueEntity(fields="email", message="A megadott e-mail cím már használatban van.")
 */
class User
    extends \FOS\UserBundle\Entity\User
    implements CharacterInterface, NotificationOwnerInterface, CharacterRecipientInterface, ApiKeyOwnerInterface
{
    use CharacterKeysTrait
    {
        CharacterKeysTrait::__construct as private __keysTraitConstruct;
    }
    use NotificationOwnerTrait
    {
        NotificationOwnerTrait::__construct as private __notificationOwnerTraitConstruct;
    }

    /**
     * @var integer $id
     *
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     */
    protected $id;

    /**
     * @var Corporation
     * @ORM\ManyToOne(targetEntity="Corporation", inversedBy="members", cascade={"persist"})
     * @ORM\JoinColumn(name="corporation_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $corporation;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\TeamSpeakClient", mappedBy="user", orphanRemoval=true)
     */
    protected $teamSpeakClients;

    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="Mail", mappedBy="toCharacters")
     */
    protected $mails;

    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="MailingList", mappedBy="memberCharacters")
     */
    protected $mailingLists;

    /**
     * @var boolean
     * @ORM\Column(name="director", type="boolean", options={"default":"0"})
     */
    protected $director = false;

    /**
     * @var DateTimeInterface
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    protected $updatedAt;

    /**
     * @var string
     * @ORM\Column(name="eve_sso_token", type="string", nullable=true)
     */
    protected $eveSSOToken;

    /**
     * Constructor
     *
     * @param int $characterID
     */
    public function __construct($characterID = 0)
    {
        parent::__construct();
        self::__keysTraitConstruct();
        self::__notificationOwnerTraitConstruct();
        $this->setId($characterID);
        $this->teamSpeakClients = new ArrayCollection();
        $this->mails            = new ArrayCollection();
        $this->updatedAt        = new DateTime('now');
    }

    /**
     * @param int $characterID
     *
     * @return $this
     */
    public function setId($characterID)
    {
        $this->id = $characterID;

        return $this;
    }

    /**
     * @param string $name
     *
     * @return $this
     */
    public function setName($name)
    {
        $this->setUsername($name);
        $this->setEmail($name);
        $this->setPassword(uniqid());

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->getUsername();
    }

    /**
     * @return Corporation
     */
    public function getCorporation()
    {
        return $this->corporation;
    }

    /**
     * @param CorporationInterface $corporation
     *
     * @return $this
     */
    public function setCorporation(CorporationInterface $corporation)
    {
        $this->corporation = $corporation;

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getTeamSpeakClients()
    {
        return $this->teamSpeakClients;
    }

    /**
     * @param TeamSpeakClient $client
     *
     * @return $this
     */
    public function addTeamSpeakClient(TeamSpeakClient $client)
    {
        $client->setUser($this);
        $this->teamSpeakClients->add($client);

        return $this;
    }

    /**
     * @param TeamSpeakClient $client
     *
     * @return $this
     */
    public function removeTeamSpeakClient(TeamSpeakClient $client)
    {
        $this->teamSpeakClients->removeElement($client);
        $client->setUser(null);

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getMails()
    {
        return $this->mails;
    }

    /**
     * @param MailInterface $mail
     *
     * @return $this
     */
    public function addMail(MailInterface $mail)
    {
        if (!$this->mails->contains($mail))
        {
            $this->mails->add($mail);
            $mail->addToCharacter($this);
        }

        return $this;
    }

    /**
     * @param MailInterface $mail
     *
     * @return $this
     */
    public function removeMail(MailInterface $mail)
    {
        $this->mails->removeElement($mail);
        $mail->removeToCharacter($this);

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getMailingLists()
    {
        return $this->mailingLists;
    }

    /**
     * @param MailingListInterface $mailingList
     *
     * @return $this
     */
    public function addMailingList(MailingListInterface $mailingList)
    {
        if (!$this->mailingLists->contains($mailingList))
        {
            $this->mailingLists->add($mailingList);
            $mailingList->addMemberCharacter($this);
        }

        return $this;
    }

    /**
     * @param MailingListInterface $mailingList
     *
     * @return $this
     */
    public function removeMailingList(MailingListInterface $mailingList)
    {
        $this->mailingLists->removeElement($mailingList);
        $mailingList->removeMemberCharacter($this);

        return $this;
    }

    /**
     * @return boolean
     */
    public function isDirector()
    {
        return $this->director;
    }

    /**
     * @param boolean $director
     */
    public function setDirector($director)
    {
        $this->director = $director;
    }

    /**
     * @return DateTimeInterface
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param DateTimeInterface $updatedAt
     *
     * @return $this
     */
    public function setUpdatedAt(DateTimeInterface $updatedAt = null)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getEveSSOToken()
    {
        return $this->eveSSOToken;
    }

    /**
     * @param mixed $eveSSOToken
     *
     * @return $this
     */
    public function setEveSSOToken($eveSSOToken)
    {
        $this->eveSSOToken = $eveSSOToken;

        return $this;
    }

    /**
     * @return bool
     */
    public function isCeo()
    {
        if ($this->getCorporation()->getCeo())
        {
            return $this->id == $this->getCorporation()->getCeo()->getId();
        }
        else
        {
            return false;
        }
    }
}
