<?php

namespace UserBundle\Handler;


use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use JMS\DiExtraBundle\Annotation as DI;


/**
 * @DI\Service("user.forced_logout_handler")
 */
class ForcedLogoutHandler
{
    const REQUEST_ATTRIBUTE = '_force_logout';

    /** @var TokenStorageInterface */
    private $tokenStorage;

    /** @var RequestStack */
    private $requestStack;

    /** @var string */
    private $sessionName;

    /** @var string */
    private $rememberName;

    /**
     * @DI\InjectParams({
     *     "tokenStorage" = @DI\Inject("security.token_storage"),
     *     "requestStack" = @DI\Inject("request_stack"),
     *     "sessionName"  = @DI\Inject("%session.name%"),
     *     "rememberName" = @DI\Inject("%session.remember_me.name%")
     * })
     *
     * @param TokenStorageInterface $tokenStorage
     * @param RequestStack          $requestStack
     * @param string                $sessionName
     * @param string                $rememberName
     */
    public function __construct(
        TokenStorageInterface $tokenStorage,
        RequestStack $requestStack,
        $sessionName,
        $rememberName
    ) {
        $this->tokenStorage = $tokenStorage;
        $this->requestStack = $requestStack;
        $this->sessionName = $sessionName;
        $this->rememberName = $rememberName;
    }

    /**
     * Makes the current user logged out
     */
    public function forceLogoutUser()
    {
        $masterRequest = $this->requestStack->getMasterRequest();

        $masterRequest->getSession()->invalidate();
        $masterRequest->attributes->set(self::REQUEST_ATTRIBUTE, true);

        $this->tokenStorage->setToken(null);
    }

    /**
     * @DI\Observe("kernel.response")
     *
     * @param FilterResponseEvent $event
     */
    public function onKernelResponse(FilterResponseEvent $event)
    {
        if ($event->getRequest()->attributes->has(self::REQUEST_ATTRIBUTE))
        {
            $response = $event->getResponse();
            $headers = $response->headers;

            $headers->clearCookie($this->sessionName);
            $headers->clearCookie($this->rememberName);
        }
    }
}
