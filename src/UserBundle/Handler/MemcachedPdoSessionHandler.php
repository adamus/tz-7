<?php


namespace UserBundle\Handler;

use Symfony\Component\HttpFoundation\Session\Storage\Handler\MemcachedSessionHandler;
use Symfony\Component\HttpFoundation\Session\Storage\Handler\PdoSessionHandler;

/**
 *
 */
class MemcachedPdoSessionHandler implements \SessionHandlerInterface
{
    /**
     * @var MemcachedSessionHandler
     */
    protected $ms;

    /**
     * @var PdoSessionHandler
     */
    protected $ps;

    /**
     * @param \Memcached $memcached A \Memcached instance
     * @param array      $options   An associative array of Memcached options
     * @param \PDO $pdo
     * @param array $dbOptions
     */
    public function __construct(\Memcached $memcached, array $options = array(), \PDO $pdo, array $dbOptions = array())
    {
        $this->ms = new MemcachedSessionHandler($memcached, $options);
        $this->ps = new PdoSessionHandler($pdo, $dbOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function open($savePath, $sessionName)
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function close()
    {
        $this->ps->close();

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function read($sessionId)
    {
        $data = $this->ms->read($sessionId);
        if (!$data) {
            $data = $this->ps->read($sessionId);
            if ($data) {
                $this->ms->write($sessionId, $data);
            }
        }
        return $data;
    }

    /**
     * {@inheritdoc}
     */
    public function write($sessionId, $data)
    {
        if ($data !== $this->read($sessionId)) {
            $this->ms->write($sessionId, $data);
            return $this->ps->write($sessionId, $data);
        } else {
            $this->ms->write($sessionId, $data);
            return true;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function destroy($sessionId)
    {
        $memcachedDestroy = $this->ms->destroy($sessionId);
        $pdoDestroy = $this->ps->destroy($sessionId);
        return $memcachedDestroy || $pdoDestroy;
    }

    /**
     * {@inheritdoc}
     */
    public function gc($maxlifetime)
    {
        $this->ms->gc($maxlifetime);
        $this->ps->gc($maxlifetime);
        return true;
    }
}

