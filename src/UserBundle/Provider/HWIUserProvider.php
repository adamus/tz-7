<?php

namespace UserBundle\Provider;


use FOS\UserBundle\Doctrine\UserManager;
use HWI\Bundle\OAuthBundle\OAuth\Response\UserResponseInterface;
use HWI\Bundle\OAuthBundle\Security\Core\Exception\AccountNotLinkedException;
use HWI\Bundle\OAuthBundle\Security\Core\User\FOSUBUserProvider as BaseClass;
use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Bridge\Monolog\Logger;
use Symfony\Component\Security\Core\User\UserInterface;
use Tz7\EveApiBundle\Service\PublicAPI;
use UserBundle\Entity\User;
use UserBundle\Handler\ForcedLogoutHandler;


/**
 * @DI\Service("hwi_user_provider")
 */
class HWIUserProvider extends BaseClass
{
    /** @var ForcedLogoutHandler */
    protected $forcedLogout;

    /** @var PublicAPI */
    protected $api;

    /** @var Logger */
    protected $logger;

    /** @var string */
    protected $emailPattern;

    /**
     * @DI\InjectParams({
     *      "userManager"            = @DI\Inject("fos_user.user_manager"),
     *      "api"                    = @DI\Inject("tz7.eve_api.public"),
     *      "forcedLogout"           = @DI\Inject("user.forced_logout_handler"),
     *      "logger"                 = @DI\Inject("logger"),
     *      "properties"             = @DI\Inject("%hwi_fosub_properties%"),
     *      "emailPattern"           = @DI\Inject("%eve_online_sso_email%")
     * })
     *
     * @param UserManager         $userManager
     * @param PublicAPI           $api
     * @param ForcedLogoutHandler $forcedLogout
     * @param Logger              $logger
     * @param array               $properties
     * @param string              $emailPattern
     */
    public function __construct(
        UserManager $userManager,
        PublicAPI $api,
        ForcedLogoutHandler $forcedLogout,
        Logger $logger,
        array $properties,
        $emailPattern
    ) {
        parent::__construct($userManager, $properties);

        $this->forcedLogout = $forcedLogout;
        $this->api = $api;
        $this->logger = $logger;
        $this->emailPattern = $emailPattern;
    }

    /**
     * {@inheritDoc}
     */
    public function connect(UserInterface $user, UserResponseInterface $response)
    {
        $property = $this->getProperty($response);
        $username = $response->getUsername();
        $this->logger->debug(sprintf('%s: "%s"', __METHOD__, $username));

        //on connect - get the access token and the user ID
        $service = $response->getResourceOwner()->getName();

        $setter = 'set' . ucfirst($service);
        $setter_id = $setter . 'Id';
        $setter_token = $setter . 'AccessToken';

        //we "disconnect" previously connected users //@TODO
        if (null !== $previousUser = $this->userManager->findUserBy([$property => $username]))
        {
            $previousUser->$setter_id(null);
            $previousUser->$setter_token(null);
            $this->userManager->updateUser($previousUser);
        }

        //we connect current user
        $user->$setter_id($username);
        $user->$setter_token($response->getAccessToken());

        /** @var User $user */
        $this->userManager->updateUser($user);
    }

    /**
     * {@inheritdoc}
     */
    public function loadUserByOAuthUserResponse(UserResponseInterface $response)
    {
        $service = $response->getResourceOwner()->getName();

        switch ($service)
        {
            case 'eve_online':
                return $this->loadUserByCCPResponse($response);
        }

        return null;
    }

    /**
     * @param UserResponseInterface $response
     *
     * @return User
     */
    protected function loadUserByCCPResponse(UserResponseInterface $response)
    {
        $characterId = $response->getUsername();
        $characterName = $response->getNickname();
        $this->logger->debug(sprintf('%s: "%s", "%s"', __METHOD__, $characterId, $characterName));

        if ($characterId)
        {
            $user = $this->userManager->findUserBy(['id' => $characterId]);
        }
        else
        {
            $this->logger->debug(sprintf('%s: Find user by token "%s"', __METHOD__, $response->getAccessToken()));
            $user = $this->userManager->findUserBy(['eveSSOToken' => $response->getAccessToken()]);
        }

        $email = sprintf($this->emailPattern, $characterId);
        if (!$user && !$characterId)
        {
            $this->logger->error(sprintf('%s::oAuth User not found and characterId is not provided', __METHOD__));
            $this->forcedLogout->forceLogoutUser();
        }
        elseif (!$user)
        {
            $user = new User($characterId);
            $user->setUsername($characterName);
            $user->setEmail($email);
            $user->setPassword($response->getAccessToken());

            try
            {
                $this->api->updateCharacterPublic($user);
            }
            catch (\Exception $ex)
            {
                $this->logger->error(sprintf('%s::API "%s"', __METHOD__, $ex->getMessage()));
                throw new AccountNotLinkedException('CCP: Character API failure');
            }

            $this->userManager->updateUser($user);
        }

        if ($user->getEveSSOToken() != $response->getAccessToken())
        {
            $user->setEveSSOToken($response->getAccessToken());
            $user->setEnabled(true);
            $this->userManager->updateUser($user);
        }

        return $user;
    }
}
