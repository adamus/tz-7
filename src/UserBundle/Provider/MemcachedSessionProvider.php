<?php


namespace UserBundle\Provider;


if (class_exists('\Memcached')) {
    class MemcachedSessionProvider extends \Memcached
    {
        /**
         * http://stackoverflow.com/questions/29118446/too-many-open-files-when-using-memcached-for-sessions
         *
         * @param string $host
         * @param int $port
         * @param int $weight
         * @return bool
         */
        public function addServer($host, $port, $weight = 0)
        {
            if (!count($this->getServerList())) {
                return parent::addServer($host, $port, $weight);
            }
            return true;
        }
    }
} else {
    class MemcachedSessionProvider
    {
        public function __construct()
        {
            throw new \InvalidArgumentException('Class "Memcached" does not exist.');
        }
    }
}

