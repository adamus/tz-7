;(function($, window){

    $(document).ready(function(){

        $('[data-auto-refresh]').each(function(){
            var $this = $(this);
            var seconds = $this.data('auto-refresh');
            var $counter = $('[data-counter]', $(this));
            setInterval(function(){
                seconds--;
                if (seconds<=0) {
                    window.location.reload();
                } else {
                    $counter.html(seconds);
                }
            }, 1000);
        });

    });

})(jQuery, window);